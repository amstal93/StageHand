# Create a new database cluster
resource "digitalocean_database_cluster" "stagehand" {
  lifecycle {
    prevent_destroy = true
  }

  name       = "stagehand"
  engine     = "pg"
  version    = "11"
  size       = "db-s-1vcpu-1gb"
  region     = var.region
  node_count = 1
}

