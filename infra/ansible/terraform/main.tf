terraform {
  backend "s3" {
    bucket                      = "terraform-stagehand"
    endpoint                    = "sgp1.digitaloceanspaces.com"
    key                         = "terraform.tfstate"
    region                      = "us-west-1"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}

variable "do_token" {
}

variable "spaces_access_id" {
}

variable "spaces_secret_key" {
}

variable "domain" {
  default = "stagehand.dev"
}

variable "region" {
  default = "sgp1"
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  spaces_access_id  = var.spaces_access_id
  spaces_secret_key = var.spaces_secret_key
  token             = var.do_token
  version           = "1.22.2"
}

