resource "digitalocean_domain" "stagehand" {
  name = var.domain
}

resource "digitalocean_record" "root" {
  domain = digitalocean_domain.stagehand.name
  type   = "A"
  name   = "@"
  value  = digitalocean_droplet.app.ipv4_address
}

resource "digitalocean_record" "www" {
  domain = digitalocean_domain.stagehand.name
  type   = "A"
  name   = "www"
  value  = digitalocean_droplet.app.ipv4_address
}

resource "digitalocean_record" "astoria" {
  domain = digitalocean_domain.stagehand.name
  type   = "A"
  name   = "astoria"
  value  = digitalocean_droplet.app.ipv4_address
}

resource "digitalocean_record" "academy" {
  domain = digitalocean_domain.stagehand.name
  type   = "A"
  name   = "academy"
  value  = digitalocean_droplet.app.ipv4_address
}

resource "digitalocean_record" "previews_wildcard" {
  domain = digitalocean_domain.stagehand.name
  type   = "A"
  name   = "*"
  value  = "188.166.198.200"
}

