---
- name: Setup infrastructure
  hosts: localhost
  connection: local
  tasks:
    - name: Terraform
      delegate_to: localhost
      register: terraform
      terraform:
        project_path: './terraform'
        force_init: true
        backend_config:
          access_key: '{{spaces_access_token}}'
          secret_key: '{{spaces_access_key}}'
        state: present
        variables:
          do_token: '{{do_token}}'
          spaces_access_id: '{{spaces_access_token}}'
          spaces_secret_key: '{{spaces_access_key}}'

    - name: Add app to the hosts file
      add_host:
        name: '{{ terraform.outputs.app_ip.value }}'
        groups: nodes

- name: Setup provisioned nodes
  hosts: nodes
  tasks:
    - name: Update packages
      apt:
        upgrade: yes
        update_cache: yes
        cache_valid_time: 86400

    - name: Install pip3
      apt:
        name: python3-pip

    - pip:
        name: docker
        executable: /usr/bin/pip3

    - import_role:
        name: geerlingguy.docker

    - import_role:
        name: andrewsomething.do-agent

    - name: Cleanup docker images
      cron:
        name: 'docker cleanup'
        minute: '0'
        hour: '0'
        job: 'docker system prune -fa > /dev/null'

    - name: Copy config to remote
      copy:
        src: './support/Caddyfile'
        dest: '/root/Caddyfile'
        mode: 0600

    - name: Create websites network
      docker_network:
        name: websites

    - name: Start reverse proxy
      docker_container:
        name: caddy
        image: caddy:2.1.1
        log_driver: json-file
        restart_policy: unless-stopped
        log_options:
          'max-size': '5m'
          'max-file': '3'
        state: started
        published_ports:
          - 80:80
          - 443:443
        env:
          BITBUCKET_CLIENT_ID: '{{bitbucket_client_id}}'
        volumes:
          - 'caddy_data:/data'
          - 'caddy_config:/config'
          - '/root/Caddyfile:/etc/caddy/Caddyfile'
        networks:
          - name: websites
