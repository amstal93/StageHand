---
- name: Setup infrastructure
  hosts: localhost
  connection: local
  tasks:
    - name: Terraform
      delegate_to: localhost
      register: terraform
      terraform:
        project_path: './terraform'
        force_init: true
        backend_config:
          access_key: '{{spaces_access_token}}'
          secret_key: '{{spaces_access_key}}'
        state: present
        variables:
          do_token: '{{do_token}}'
          spaces_access_id: '{{spaces_access_token}}'
          spaces_secret_key: '{{spaces_access_key}}'

    - name: Add app to the hosts file
      add_host:
        name: '{{ terraform.outputs.app_ip.value }}'
        groups: nodes

    - name: Create ssh directory
      file:
        path: '{{lookup("env","HOME")}}/.ssh'
        state: directory

    - name: Output key
      shell: 'ansible-vault view ./support/private_key.enc > {{lookup("env","HOME")}}/.ssh/id_ed25519'
      changed_when: false

    - name: Protect key
      file:
        path: '/{{lookup("env","HOME")}}/.ssh/id_ed25519'
        mode: '0400'

- name: Deploy the server
  hosts: nodes
  tasks:
    - name: Copy production kube config to remote
      copy:
        src: './support/kubeconfig.prod.enc'
        dest: '/root/kubeconfig.prod'
        decrypt: yes
        mode: 0600

    - name: Login to registry
      docker_login:
        registry: registry.gitlab.com
        username: Rodeoclash
        password: '{{gitlab_pa_token}}'
        reauthorize: yes

    - name: Force pull the image
      docker_image:
        name: '{{lookup("env","DEPLOY_IMAGE")}}'
        force: true
        source: pull

    - name: Start app
      docker_container:
        name: stagehand
        image: '{{lookup("env","DEPLOY_IMAGE")}}'
        log_driver: json-file
        restart_policy: unless-stopped
        log_options:
          'max-size': '5m'
          'max-file': '3'
        state: started
        env:
          BITBUCKET_CLIENT_ID: '{{bitbucket_client_id}}'
          BITBUCKET_CLIENT_SECRET: '{{bitbucket_client_secret}}'
          DATABASE_URL: '{{database_url}}'
          GITHUB_CLIENT_ID: '{{github_client_id}}'
          GITHUB_CLIENT_SECRET: '{{github_client_secret}}'
          GUARDIAN_SECRET_KEY: '{{guardian_secret_key}}'
          MIX_ENV: 'prod'
          PORT: '4000'
          PREVIEW_DOMAIN: 'https://stagehand.dev'
          SECRET_KEY_BASE: '{{secret_key_base}}'
          SENTRY_DSN: 'https://7846d08476b54809bfd503015b2c0def@sentry.io/1806825'
        volumes:
          - '/root/kubeconfig.prod:/opt/app/config/kubeconfig.prod'
        networks:
          - name: websites
