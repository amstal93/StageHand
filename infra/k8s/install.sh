#!/bin/bash

set -e

# ------------------------------------
# Install certbot
# ------------------------------------

echo "--- installing cert-manager"
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.3/cert-manager.yaml

echo "--- adding DO access token"
kubectl create secret generic digitalocean-dns --from-file=./cert-manager/access-token --namespace cert-manager

# ------------------------------------
# Install ambassador
# ------------------------------------

echo "--- installing ambassador"
#kubectl apply -f https://getambassador.io/yaml/ambassador/ambassador-rbac.yaml
kubectl apply -f https://www.getambassador.io/yaml/ambassador/ambassador-crds.yaml
kubectl apply -f https://www.getambassador.io/yaml/ambassador/ambassador-rbac.yaml
kubectl scale deployment ambassador --replicas=1
kubectl apply -f ./ambassador/service.yml

echo "--- waiting for load balancer to be assigned"
sleep 5m

echo "Setup domain before proceeding"
read -p "Press enter to continue"

# ------------------------------------
# Add certificate issuer
# ------------------------------------

echo "--- installing cert-manager issuer"
kubectl apply -f ./cert-manager/issuer.yml

echo "Check progress with: kubectl describe certificates kubernetes-stagehand-certificate"

# ------------------------------------
# Install stagehand
# ------------------------------------

echo "--- create stagehand namespace"
kubectl create namespace stagehand

echo "--- create stagehand service account"
kubectl apply -f ./stagehand/service_account.yml

echo "--- fetching service account details"
SERVICE_ACCOUNT_NAME=`kubectl get serviceaccount stagehand --namespace=stagehand -o json | jq -Mr '.secrets[].name'`
SERVICE_ACCOUNT_TOKEN=`kubectl get secrets $SERVICE_ACCOUNT_NAME --namespace=stagehand -o json | jq -Mr '.data.token' | base64 -d`
SERVICE_ACCOUNT_CA=`kubectl get secrets $SERVICE_ACCOUNT_NAME --namespace=stagehand -o json | jq -Mr '.data["ca.crt"]' | base64 -d`

echo "## NAME ##"
echo $SERVICE_ACCOUNT_NAME

echo "## TOKEN ##"
echo $SERVICE_ACCOUNT_TOKEN

echo "## CA ##"
printf "%s\n" "$SERVICE_ACCOUNT_CA"

echo "## CA Base64 Encoded ##"
printf "%s\n" "$SERVICE_ACCOUNT_CA" | base64
