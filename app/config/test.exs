import Config

config :stagehand,
  http_client: App.HTTPoisonMock,
  kubernetes_resource_poll_interval: 500,
  kubernetes_v2: App.KubernetesV2Mock,
  run_monitor: false,
  utils: App.UtilsMock

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :stagehand, AppWeb.Endpoint,
  http: [port: 4001],
  url: [
    host: "stagehand-test.com",
    port: 80,
    scheme: "http"
  ],
  server: true

# Configure your database
config :stagehand, App.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "app_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :stagehand, :environment, :test

config :stagehand, :sql_sandbox, true

config :sentry,
  dsn: "https://7846d08476b54809bfd503015b2c0def@sentry.io/1806825",
  environment_name: :test,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!,
  tags: %{
    env: "test"
  },
  included_environments: [:prod]

config :ueberauth, Ueberauth.Strategy.Bitbucket.OAuth,
  client_id: "1234",
  client_secret: "abcd"

config :logger, level: :info
