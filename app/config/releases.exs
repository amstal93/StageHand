import Config

config :stagehand, App.Repo,
  url: System.fetch_env!("DATABASE_URL")

config :stagehand,
  preview_domain: System.fetch_env!("PREVIEW_DOMAIN")

config :ueberauth, Ueberauth.Strategy.Bitbucket.OAuth,
  client_id: System.fetch_env!("BITBUCKET_CLIENT_ID"),
  client_secret: System.fetch_env!("BITBUCKET_CLIENT_SECRET")

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.fetch_env!("GITHUB_CLIENT_ID"),
  client_secret: System.fetch_env!("GITHUB_CLIENT_SECRET")

config :sentry,
  dsn: System.fetch_env!("SENTRY_DSN")

config :stagehand, AppWeb.Endpoint,
  secret_key_base: System.fetch_env!("SECRET_KEY_BASE")

config :stagehand, App.Guardian,
  secret_key: System.fetch_env!("GUARDIAN_SECRET_KEY")
