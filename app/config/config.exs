import Config

# General application configuration
config :stagehand,
  ecto_repos: [App.Repo],
  generators: [binary_id: true],
  http_client: HTTPoison,
  kubernetes_resource_poll_interval: 60000,
  kubernetes_v2: App.KubernetesV2,
  preview_domain: "http://stagehand.home:32568",
  run_monitor: true,
  utils: App.Utils

# Configures the endpoint
config :stagehand, AppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "26GVUYsQNcHIxg2e4iHQEMIPdJbU2dukaScTipzWem9wNSCxzL9xB5JCkYkTgLiz",
  render_errors: [view: AppWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: App.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :stagehand, App.Guardian,
  issuer: "stagehand",
  secret_key: "oYkV00SpQWOriLb3gO+b94AbgfYCqOYX5GFuXnKYjMX6LKc4IuUlxg+v0xpF4L4R"

config :ueberauth, Ueberauth,
  providers: [
    bitbucket: {Ueberauth.Strategy.Bitbucket, []},
    github: {Ueberauth.Strategy.Github, [default_scope: "user,repo"]}
  ]

# TODO: This might be able to be removed
config :ueberauth, Ueberauth.Strategy.Bitbucket.OAuth,
  client_id: "1234",
  client_secret: "abcd"

import_config "#{Mix.env()}.exs"
