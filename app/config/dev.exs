import Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :stagehand, AppWeb.Endpoint,
  http: [
    port: 4000
  ],
  url: [
    host: "stagehand.au.ngrok.io",
    port: 80,
    scheme: "http"
  ],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      "development",
      "--watch-stdin",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

# Watch static and templates for browser reloading.
config :stagehand, AppWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/app_web/views/.*(ex)$},
      ~r{lib/app_web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"
config :logger, level: :debug

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :stagehand, App.Repo,
  username: "postgres",
  password: "postgres",
  database: "app_dev",
  hostname: "postgres",
  pool_size: 5


config :stagehand, :environment, :dev
config :oauth2, debug: true

# Configure k8s
config :k8s,
  clusters: %{
    default: %{
      conn: "config/kubeconfig.dev"
    }
  }

config :sentry,
  dsn: "https://7846d08476b54809bfd503015b2c0def@sentry.io/1806825",
  environment_name: :dev,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!,
  tags: %{
    env: "development"
  },
  included_environments: [:prod]

import_config "dev.secret.exs"
