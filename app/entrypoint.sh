#!/bin/bash
set -e

/opt/app/bin/stagehand eval "App.Release.migrate"

exec "$@"
