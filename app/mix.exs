defmodule App.Mixfile do
  use Mix.Project

  def project do
    [
      aliases: aliases(),
      app: :stagehand,
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      deps: deps(),
      elixir: "~> 1.8",
      elixirc_paths: elixirc_paths(Mix.env()),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      releases: [
        stagehand: [
          include_executables_for: [:unix],
          applications: [runtime_tools: :permanent]
        ]
      ],
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      version: "0.0.7"
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {App.Application, []},
      extra_applications: [
        :logger,
        :runtime_tools,
        :ssl,
        :ueberauth_bitbucket,
        :ueberauth_github
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:cowboy, "~> 2.6"},
      {:ecto_sql, "~> 3.0"},
      {:ex_machina, "~> 2.3"},
      {:excoveralls, "~> 0.11", only: :test},
      {:gettext, "~> 0.17"},
      {:guardian, "~> 2.0"},
      {:httpoison, "~> 1.5"},
      {:k8s, "~> 0.4"},
      {:mox, "~> 0.5", only: :test},
      {:phoenix, "~> 1.4"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_gon, "~> 0.4"},
      {:phoenix_html, "~> 2.13"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_pubsub, "~> 1.0"},
      {:plug_cowboy, "~> 2.1"},
      {:poison, "~> 3.1"},
      {:postgrex, "~> 0.15"},
      {:sentry, "~> 7.0"},
      {:ueberauth_bitbucket, "~> 1.0"},
      {:ueberauth_github, "~> 0.7"},
      {:websockex, "~> 0.4"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
