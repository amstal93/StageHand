import { Socket as PhoenixSocket } from 'phoenix'

export const getSocket = token => {
  const socket = new PhoenixSocket('/socket', { params: { token } })
  socket.connect()
  return socket
}

export const getChannel = (socket, name) => {
  const channel = socket.channel(name)
  channel.join()
  return channel
}
