import { observable, action } from 'mobx'
import { every } from 'lodash/fp'

const store = observable({
  description: window.Gon.getAsset('description'),
  hasLaunched: false,
  launch: window.Gon.getAsset('launch'),
  ports: window.Gon.getAsset('ports'),
  preview: window.Gon.getAsset('preview'),
  requestingLogs: false,
  selectedPort: window.Gon.getAsset('ports')[0],
  state: 'setup',

  get finishedSetup() {
    return (
      this.preview.provisioning.docker === true &&
      this.preview.status.code_fetched === true &&
      this.preview.status.code_extracted === true
    )
  },

  get previewsReady() {
    return every(({ status: status }) => {
      return status === 'ready'
    })(this.preview.readiness.toJS())
  },

  get showingLogs() {
    return this.state === 'bootup' || this.requestingLogs
  }
})

export default store
