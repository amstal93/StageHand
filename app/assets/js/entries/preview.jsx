import * as React from 'react'
import { isBoolean } from 'lodash/fp'
import renderApp from '@/services/dom.js'
import { getSocket, getChannel } from '@/services/channel.js'

import store from '@/entries/preview/store.js'

import Preview from '@/components/Preview/Preview.jsx'

const channelToken = window.Gon.getAsset('channel_token')
const channelName = window.Gon.getAsset('channel_name')
const previewSocket = getSocket(channelToken)
const previewChannel = getChannel(previewSocket, channelName)

if (Notification.permission !== 'denied') {
  Notification.requestPermission()
}

const withStateUpdate = cb => {
  if (cb) {
    cb()
  }

  // from setup

  if (
    store.state === 'setup' &&
    store.finishedSetup === true &&
    store.previewsReady === false
  ) {
    store.state = 'bootup'
  }

  if (
    store.state === 'setup' &&
    store.finishedSetup === true &&
    store.previewsReady === true
  ) {
    store.state = 'previewing'
  }

  // from bootup

  if (
    store.state === 'bootup' &&
    store.finishedSetup === true &&
    store.previewsReady === true
  ) {
    if (store.hasLaunched === false) {
      store.hasLaunched = true
      if (Notification.permission === 'granted') {
        new Notification('Your Stagehand preview has launched')
      }
    }

    store.state = 'previewing'
  }
}

withStateUpdate()

// Fired after a status change in the preview pod
previewChannel.on('preview', ({ data }) => {
  withStateUpdate(() => {
    if (store.preview === null) {
      store.preview = {}
    }

    store.preview.provisioning = data.provisioning
    store.preview.readiness = data.readiness
    store.preview.status = data.status
  })
})

// Fired on log line being added
previewChannel.on('log', ({ data: log }) => {
  if (store.preview.logs === undefined) {
    store.preview.logs = log
  } else {
    store.preview.logs = store.preview.logs + log
  }
})

// Register that we're viewing the preview
setInterval(() => {
  previewChannel.push('heartbeat')
}, 5000)

// Mount application
document.getElementById('loading').remove()
renderApp(<Preview />)
