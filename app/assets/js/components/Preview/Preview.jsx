import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import PreviewLaunchMessage from '@/components/PreviewLaunchMessage/PreviewLaunchMessage.jsx'
import PreviewLoad from '@/components/PreviewLoad/PreviewLoad.jsx'
import PreviewLogs from '@/components/PreviewLogs/PreviewLogs.jsx'
import PreviewSetup from '@/components/PreviewSetup/PreviewSetup.jsx'
import PreviewShow from '@/components/PreviewShow/PreviewShow.jsx'
import PreviewSidebar from '@/components/PreviewSidebar/PreviewSidebar.jsx'

import styles from './styles.css'

const Preview = () => {
  return useObserver(() => {
    const renderedContent = (() => {
      if (store.launch[0] === 'error') {
        return <PreviewLaunchMessage />
      }

      if (store.state === 'loading') {
        return <PreviewLoad />
      }

      if (store.state === 'setup') {
        return <PreviewSetup />
      }

      if (store.state === 'bootup' || store.showingLogs === true) {
        return (
          <article className={styles.article}>
            <PreviewLogs />
          </article>
        )
      }

      if (store.state === 'previewing') {
        return (
          <article className={styles.article}>
            <PreviewShow />
          </article>
        )
      }
    })()

    const renderedSidebar = (() => {
      if (store.state === 'bootup' || store.state === 'previewing') {
        return (
          <aside className={styles.aside}>
            <PreviewSidebar />
          </aside>
        )
      }

      return null
    })()

    return (
      <>
        <main className={styles.root}>
          {renderedSidebar}
          {renderedContent}
        </main>
      </>
    )
  })
}

export default Preview
