import * as React from 'react'
import { FiSquare, FiCheckSquare } from 'react-icons/fi'
import classNames from 'classnames'

import styles from './styles.css'

export const PreviewSetupStatus = props => {
  const { title, completed } = props
  const renderedIcon = completed === false ? <FiSquare /> : <FiCheckSquare />
  const classes = classNames({
    [styles.complete]: completed === true,
    [styles.root]: true
  })

  return (
    <div className={classes}>
      <span className={styles.icon}>{renderedIcon}</span> {title}
    </div>
  )
}

export default PreviewSetupStatus
