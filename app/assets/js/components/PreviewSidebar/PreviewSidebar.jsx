import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import PreviewDescription from '@/components/PreviewDescription/PreviewDescription.jsx'
import PreviewSelect from '@/components/PreviewSelect/PreviewSelect.jsx'
import PreviewShowLogs from '@/components/PreviewShowLogs/PreviewShowLogs.jsx'

import styles from './styles.css'

export const PreviewSidebar = () => {
  return useObserver(() => {
    return (
      <aside className={styles.sidebar}>
        <h1 className={styles.logo}>Stagehand</h1>
        <PreviewSelect />
        <PreviewShowLogs />
        <PreviewDescription />
      </aside>
    )
  })
}

export default PreviewSidebar
