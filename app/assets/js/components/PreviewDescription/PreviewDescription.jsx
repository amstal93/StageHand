import * as React from 'react'
import { useObserver } from 'mobx-react-lite'
import ReactMarkdown from 'react-markdown'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

export const PreviewDescription = () => {
  return useObserver(() => {
    if (store.description === null) {
      return null
    }

    return (
      <div className={styles.root}>
        <div className={styles.about}>About this preview</div>
        <ReactMarkdown source={store.description} />
      </div>
    )
  })
}

export default PreviewDescription
