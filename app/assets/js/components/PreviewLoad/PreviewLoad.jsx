import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import styles from './styles.css'

export const PreviewLoad = () => {
  return useObserver(() => {
    return (
      <div className={styles.root}>
        <em>Loading...</em>
      </div>
    )
  })
}

export default PreviewLoad
