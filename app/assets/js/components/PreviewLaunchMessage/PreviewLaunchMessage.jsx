import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

export const PreviewLaunchMessage = () => {
  return useObserver(() => {
    const kind = store.launch[1]

    const message = (() => {
      if (kind === 'forbidden') {
        return 'Tried to launch preview but failed. Preview is either terminating or a permissions error was encountered!'
      }

      if (kind === 'unknown') {
        return 'An unknown error occured! This has been reported and will be looked at shortly'
      }
    })()

    return (
      <div className={styles.root}>
        <div>
          <strong className={styles.message}>{message}</strong>
        </div>
      </div>
    )
  })
}

export default PreviewLaunchMessage
