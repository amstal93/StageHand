defmodule AppWeb.ProjectController do
  alias App.{
    Projects,
    Roles,
    Providers,
    ProviderConnections,
    Users,
    Roles
  }

  use AppWeb, :controller

  def create(conn, %{"project" => project_params}) do
    current_user = Guardian.Plug.current_resource(conn)

    {:ok, project} =
      Projects.create(%{
        name: project_params["name"]
      })

    {:ok, role} =
      Roles.create(%{
        user_id: current_user.id,
        project_id: project.id
      })

    conn
    |> assign(:project, project)
    |> assign(:role, role)
    |> put_flash(:success, "Created project #{project.name}")
    |> redirect(to: current_user_path(conn, :show))
  end

  def show(conn, params) do
    current_user = Guardian.Plug.current_resource(conn)
    project = Projects.get(params["id"])

    bitbucket_provider = Providers.get(:bitbucket)
    github_provider = Providers.get(:github)

    bitbucket_provider_connection =
      ProviderConnections.build_from_or_build_by(%{
        project_id: project.id,
        provider_id: bitbucket_provider.id
      })

    github_provider_connection =
      ProviderConnections.build_from_or_build_by(%{
        project_id: project.id,
        provider_id: github_provider.id
      })

    users = Projects.Users.all(project)

    conn
    |> assign(:bitbucket_provider, bitbucket_provider)
    |> assign(:bitbucket_provider_connection, bitbucket_provider_connection)
    |> assign(:current_user, current_user)
    |> assign(:github_provider, github_provider)
    |> assign(:github_provider_connection, github_provider_connection)
    |> assign(:project, project)
    |> assign(:users, users)
    |> render("show.html", conn: conn)
  end

  def create_provider_connection(conn, %{
        "project_id" => project_id,
        "provider_connection" => provider_connection
      }) do
    project = Projects.get(project_id)
    provider = Providers.get(provider_connection["provider_id"])
    user = Users.get(provider_connection["user_id"])

    {:ok, provider_connection} =
      ProviderConnections.get_or_create_by(%{
        project_id: project.id,
        provider_id: provider.id,
        user_id: user.id
      })

    conn
    |> assign(:provider_connection, provider_connection)
    |> put_flash(:success, "Assigned #{user.email} as the user to connect to #{provider.name}")
    |> redirect(to: project_path(conn, :show, project))
  end
end
