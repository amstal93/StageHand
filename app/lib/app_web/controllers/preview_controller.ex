defmodule AppWeb.PreviewController do
  alias App.{
    Previews
  }

  import PhoenixGon.Controller
  use AppWeb, :controller

  def show(conn, params) do
    preview = Previews.get(params["id"])

    exists? = Previews.KubernetesV2.exists?(preview)

    launch =
      if exists? == false do
        Previews.launch(preview)
      else
        {:ok, :already_running}
      end

    catchup =
      if exists? == false do
        Previews.KubernetesV2.catchup_boot(preview)
      else
        Previews.KubernetesV2.catchup_with_logs(preview)
      end

    salt = Application.get_env(:stagehand, AppWeb.Endpoint)[:secret_key_base]
    channel_token = Phoenix.Token.sign(AppWeb.Endpoint, salt, nil)

    ports =
      Enum.map(preview.configuration["ports"], fn port ->
        %{
          id: Previews.KubernetesV2.Templates.port_name(preview, port),
          config: port,
          url:
            Previews.KubernetesV2.Templates.port_url(
              preview,
              port,
              Application.get_env(:stagehand, :preview_domain)
            )
        }
      end)

    conn
    |> put_gon(channel_name: AppWeb.PreviewChannel.channel_name(preview))
    |> put_gon(channel_token: channel_token)
    |> put_gon(description: preview.configuration["description"])
    |> put_gon(launch: Tuple.to_list(launch))
    |> put_gon(ports: ports)
    |> put_gon(preview: catchup)
    |> render("show.html", conn: conn)
  end
end
