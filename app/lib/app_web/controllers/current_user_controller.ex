defmodule AppWeb.CurrentUserController do
  alias App.{Roles, Repo, Projects}
  import Ecto.Query, only: [from: 2]
  use AppWeb, :controller

  def show(conn, _params) do
    current_user = Guardian.Plug.current_resource(conn)

    new_project = Projects.Project.changeset(%Projects.Project{})

    roles =
      from(role in Roles.Role,
        where: role.user_id == type(^current_user.id, :binary_id),
        preload: [:project]
      )
      |> Repo.all()

    conn
    |> assign(:current_user, current_user)
    |> assign(:new_project, new_project)
    |> assign(:roles, roles)
    |> render("show.html", conn: conn)
  end
end
