defmodule AppWeb.AuthorisationController do
  alias App.{Users}
  use AppWeb, :controller

  # Handles callback from an OAuth provider, typically Github, Bitbucket etc
  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    {:ok, user} = Users.get_or_create_by(%{email: auth.info.email})

    Users.Authorisation.find_or_create(auth, user)

    conn
    |> App.Guardian.Plug.sign_in(user)
    |> put_flash(:info, "Successfully authenticated")
    |> redirect(to: "/current_user")
  end

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate")
    |> redirect(to: "/")
  end
end
