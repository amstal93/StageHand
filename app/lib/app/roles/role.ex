defmodule App.Roles.Role do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "roles" do
    belongs_to(:project, App.Projects.Project)
    belongs_to(:user, App.Users.User)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:project_id, :user_id])
    |> validate_required([:project_id, :user_id])
    |> unique_constraint(
      :roles_project_id_user_id_index,
      name: :roles_project_id_user_id_index
    )
  end
end
