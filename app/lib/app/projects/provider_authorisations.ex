defmodule App.Projects.ProviderAuthorisations do
  alias App.{
    ProviderAuthorisations,
    ProviderConnections,
    Repo,
    Users
  }

  import Ecto.Query, only: [from: 2]

  @doc """
  Given this project, get the provider_authorisation set for it.
  """
  def get(project, provider) do
    from(provider_authorisation in ProviderAuthorisations.ProviderAuthorisation,
      join: user in Users.User,
      on: user.id == provider_authorisation.user_id,
      join: provider_connection in ProviderConnections.ProviderConnection,
      on: provider_connection.user_id == user.id,
      where: provider_authorisation.provider_id == ^provider.id,
      where: provider_connection.project_id == ^project.id
    )
    |> Repo.one()
  end
end
