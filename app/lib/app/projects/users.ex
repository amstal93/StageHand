defmodule App.Projects.Users do
  alias App.{Projects, Roles, Users, Roles, Repo}
  import Ecto.Query, only: [from: 2]

  def all(project) do
    from(user in Users.User,
      distinct: true,
      join: role in Roles.Role,
      on: role.user_id == user.id,
      join: project in Projects.Project,
      on: role.project_id == project.id,
      select: {user.email, user.id},
      where: project.id == ^project.id
    )
    |> Repo.all()
  end
end
