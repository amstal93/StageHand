defmodule App.ProviderAuthorisations.Github.Statuses do
  alias App.{ProviderAuthorisations.Github}

  def create(provider_authorisation, owner, repo, sha, body) do
    Github.request(
      provider_authorisation,
      :post,
      "/repos/#{owner}/#{repo}/statuses/#{sha}",
      body
    )
  end
end
