defmodule App.ProviderAuthorisations.Bitbucket.Statuses do
  alias App.{ProviderAuthorisations.Bitbucket}

  def create(provider_authorisation, username, repo_slug, sha, body) do
    Bitbucket.request(
      provider_authorisation,
      :post,
      "/2.0/repositories/#{username}/#{repo_slug}/commit/#{sha}/statuses/build",
      body
    )
  end
end
