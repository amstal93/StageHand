defmodule App.Providers do
  alias App.{Repo, Providers}

  @model Providers.Provider
  @struct %Providers.Provider{}

  def create!(attrs) do
    @struct
    |> @model.changeset(attrs)
    |> Repo.insert!()
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def get(:bitbucket) do
    get_by(%{
      name: "bitbucket"
    })
  end

  def get(:github) do
    get_by(%{
      name: "github"
    })
  end

  def get(id) do
    Repo.get(@model, id)
  end

  @doc """
  Is the bitbucket provider enabled?
  """
  @spec enabled?(:bitbucket) :: boolean()
  def enabled?(:bitbucket) do
    Application.get_env(:ueberauth, Ueberauth.Strategy.Bitbucket.OAuth) != nil
  end

  @doc """
  Is the github provider enabled?
  """
  @spec enabled?(:github) :: boolean()
  def enabled?(:github) do
    Application.get_env(:ueberauth, Ueberauth.Strategy.Github.OAuth) != nil
  end
end
