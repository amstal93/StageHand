defmodule App.ProviderConnections do
  alias App.{ProviderConnections, Repo}

  @model ProviderConnections.ProviderConnection
  @struct %ProviderConnections.ProviderConnection{}

  def build(attrs) do
    @struct
    |> @model.changeset(attrs)
  end

  def create(attrs) do
    build(attrs)
    |> Repo.insert()
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def get_or_create_by(attrs) do
    case get_by(attrs) do
      nil ->
        create(attrs)

      modal ->
        {:ok, modal}
    end
  end

  def build_from_or_build_by(attrs) do
    case get_by(attrs) do
      nil ->
        build(attrs)

      model ->
        build(%{
          project_id: model.project_id,
          provider_id: model.provider_id,
          user_id: model.user_id
        })
    end
  end
end
