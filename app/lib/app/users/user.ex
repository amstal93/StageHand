defmodule App.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    field(:email, :string)
    has_many(:provider_authorisations, App.ProviderAuthorisations.ProviderAuthorisation)
    has_many(:roles, App.Roles.Role)
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_required([
      :email
    ])
    |> unique_constraint(:email)
  end
end
