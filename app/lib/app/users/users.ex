defmodule App.Users do
  alias App.{Repo, Users}

  @model Users.User
  @struct %Users.User{}

  def create(attrs) do
    @struct
    |> @model.changeset(attrs)
    |> Repo.insert()
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def get(id) do
    Repo.get(@model, id)
  end

  def update(%Users.User{} = user, attrs) do
    user
    |> Users.User.changeset(attrs)
    |> Repo.update()
  end

  def get_or_create_by(attrs) do
    case get_by(attrs) do
      nil ->
        create(attrs)

      user ->
        {:ok, user}
    end
  end
end
