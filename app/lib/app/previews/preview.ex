defmodule App.Previews.Preview do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "previews" do
    belongs_to(:project, App.Projects.Project)
    belongs_to(:provider, App.Providers.Provider)

    field(:configuration, :map, default: %{})
    field(:repo_slug, :string)
    field(:sha, :string)
    field(:username, :string)
    field(:version, :integer, default: 1)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :configuration,
      :project_id,
      :provider_id,
      :repo_slug,
      :sha,
      :username,
      :version
    ])
    |> validate_required([
      :configuration,
      :project_id,
      :provider_id,
      :repo_slug,
      :sha,
      :username,
      :version
    ])
  end
end
