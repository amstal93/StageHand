defmodule App.Previews.DynamicSupervisor.Monitor.Base do
  alias App.{
    Previews
  }

  defmacro __using__(opts) do
    monitor_name = Keyword.get(opts, :name)

    quote do
      def name(preview) do
        {unquote(monitor_name), Previews.kid(preview)}
      end

      def via_name(preview) do
        {:via, Registry, {:previews, name(preview)}}
      end

      def start_link([preview] = args) do
        log(preview, "Starting")
        GenServer.start_link(__MODULE__, args, name: via_name(preview))
      end

      @doc """
      Is the supervisor for this preview running?
      """
      def exists?(preview) do
        name(preview)
        |> Previews.DynamicSupervisor.Shared.exists?()
      end

      @doc """
      Returns pid for this server or nil if not found
      """
      def whereis(preview) do
        name(preview)
        |> Previews.DynamicSupervisor.Shared.whereis()
      end

      def log(preview, message) do
        Previews.log(preview, :info, unquote(monitor_name), message)
      end
    end
  end
end
