defmodule App.Previews.DynamicSupervisor.Monitor.Logs do
  alias App.{
    Previews
  }

  require Logger
  use WebSockex

  @kubernetes_v2 Application.get_env(:stagehand, :kubernetes_v2)

  def name(preview) do
    {:monitor_logs, Previews.kid(preview)}
  end

  def via_name(preview) do
    {:via, Registry, {:previews, name(preview)}}
  end

  # TODO: Figure out how to test this
  def conn(preview) do
    operation = Previews.KubernetesV2.Templates.Pod.logs({preview}, pod_name(preview))

    with {:ok, url} <- K8s.Cluster.url_for(operation, :default),
         {:ok, cluster_connection_config} <- K8s.Cluster.conn(:default),
         {:ok, request_options} <- K8s.Conn.RequestOptions.generate(cluster_connection_config) do
      headers =
        request_options.headers ++ [{"Accept", "*/*"}, {"Content-Type", "application/json"}]

      cacerts = Keyword.get(request_options.ssl_options, :cacerts)

      query =
        URI.encode_query(%{
          "container" => "preview",
          "follow" => "true"
        })

      WebSockex.Conn.new(url <> "?" <> query,
        cacerts: cacerts,
        extra_headers: headers,
        insecure: true,
        ssl_options: request_options.ssl_options
      )
    end
  end

  # TODO: Figure out how to test this
  def start_link([preview]) do
    log(preview, "Starting")

    WebSockex.start_link(
      conn(preview),
      __MODULE__,
      %{
        preview: preview
      },
      name: via_name(preview)
    )
  end

  def handle_frame({_type, message}, %{preview: preview} = state) do
    log(preview, "Received message, sending to client")
    AppWeb.PreviewChannel.send_message(preview, :log, message)
    {:ok, state}
  end

  def pod_name(preview) do
    {:ok, result} =
      Previews.KubernetesV2.Templates.Pod.all(preview, "preview")
      |> @kubernetes_v2.run

    result
    |> Map.get("items")
    |> List.first()
    |> Previews.KubernetesV2.Resources.Pod.name()
  end

  @doc """
  Is the supervisor for this preview running?
  """
  def exists?(preview) do
    name(preview)
    |> Previews.DynamicSupervisor.Shared.exists?()
  end

  @doc """
  Returns pid for this server or nil if not found
  """
  def whereis(preview) do
    name(preview)
    |> Previews.DynamicSupervisor.Shared.whereis()
  end

  defp log(preview, message) do
    Previews.log(preview, :info, "monitor:logs", message)
  end
end
