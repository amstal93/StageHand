defmodule App.Previews.DynamicSupervisor.Monitor do
  @moduledoc """
  Individual services to monitor a perview are attached to this supervisor (logs, health etc).
  """

  alias App.{
    KubernetesV2,
    Previews,
    Previews.DynamicSupervisor.Monitor
  }

  require Logger
  use DynamicSupervisor

  def name(preview) do
    {:monitor, KubernetesV2.kid(preview.id)}
  end

  def via_name(preview) do
    {:via, Registry, {:previews, name(preview)}}
  end

  def start_link([preview]) do
    log(preview, "Starting")

    DynamicSupervisor.start_link(
      __MODULE__,
      [],
      name: via_name(preview)
    )
  end

  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts all of the monitoring services for this preview. Logs is added later once the container is running.
  """
  def start_monitoring(preview) do
    with(
      {:ok, pid_health} <- start_module(Monitor.Health, preview),
      {:ok, pid_state} <- start_module(Monitor.State, preview),
      {:ok, pid_status} <- start_module(Monitor.Status, preview)
    ) do
      {:ok, pid_health, pid_state, pid_status}
    else
      {:error, message} -> {:error, message}
    end
  end

  @doc """
  Start a module under this supervisor
  """
  def start_module(module, preview) do
    DynamicSupervisor.start_child(whereis(preview), {module, [preview]})
  end

  @doc """
  Stop a module under this supervisor
  """
  def terminate_module(module, preview) do
    DynamicSupervisor.terminate_child(whereis(preview), module.whereis(preview))
  end

  @doc """
  Is the supervisor for this preview running?
  """
  @spec exists?(%Previews.Preview{}) :: Boolean.t()
  def exists?(preview) do
    name(preview)
    |> Previews.DynamicSupervisor.Shared.exists?()
  end

  @doc """
  Is the supervisor for this preview not running?
  """
  @spec missing?(%Previews.Preview{}) :: Boolean.t()
  def missing?(preview) do
    !exists?(preview)
  end

  @doc """
  Is this monitor running all its services?
  """
  @spec incomplete?(%Previews.Preview{}) :: Boolean.t()
  def complete?(preview) do
    exists?(preview) == true &&
      Monitor.Health.exists?(preview) == true &&
      Monitor.State.exists?(preview) == true &&
      Monitor.Status.exists?(preview) == true
  end

  @doc """
  Is this monitor missing any of its services?
  """
  @spec incomplete?(%Previews.Preview{}) :: Boolean.t()
  def incomplete?(preview) do
    !complete?(preview)
  end

  @doc """
  Returns pid for this server or nil if not found
  """
  def whereis(preview) do
    name(preview)
    |> Previews.DynamicSupervisor.Shared.whereis()
  end

  defp log(preview, message) do
    Previews.log(preview, :info, "monitor", message)
  end
end
