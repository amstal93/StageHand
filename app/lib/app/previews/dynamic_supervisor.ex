defmodule App.Previews.DynamicSupervisor do
  @moduledoc """
  Provides the top level dynamic supervisor that child dynamic supervisors can be attached to.
  """

  alias App.{Previews}
  require Logger
  use DynamicSupervisor

  def start_link do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def stop do
    DynamicSupervisor.stop(__MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Given a preview, creates a child dynamic supervisor to monitor it within K8s
  """
  def start_child(preview) do
    child_spec = {Previews.DynamicSupervisor.Monitor, [preview]}
    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Given a preview, removes monitoring it in K8s

  TODO: Think about broadcasting something to the client here
  """
  def terminate_child(preview) do
    pid = Previews.DynamicSupervisor.Monitor.whereis(preview)
    DynamicSupervisor.terminate_child(__MODULE__, pid)
  end
end
