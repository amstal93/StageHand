defmodule App.Previews.Launch do
  alias App.{
    Previews
  }

  @doc """
  Launches the preview in Kubernetes
  """
  @spec kubernetes(%Previews.Preview{}) :: String.t()
  def kubernetes(preview) do
    Previews.KubernetesV2.start(preview)
    |> Enum.find(fn el ->
      match?({:error, _}, el)
    end)
  end
end
