defmodule App.Previews.Github.RemoteRepository do
  alias App.{
    Previews
  }

  def download_url(preview) do
    "https://github.com/#{preview.username}/#{preview.repo_slug}/tarball/#{preview.sha}"
  end

  def auth_header(preview) do
    provider_authorisation = Previews.ProviderAuthorisations.get(preview)
    "Authorization: token #{provider_authorisation.token}"
  end
end
