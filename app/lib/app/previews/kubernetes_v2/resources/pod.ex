defmodule App.Previews.KubernetesV2.Resources.Pod do
  alias App.{
    Previews
  }

  # list

  @doc """
  Returns payload of preview state from list of pods in the preview
  """
  def state(pods) do
    preview_pod = preview_pod(pods)

    %{
      status: %{
        code_extracted: has_status(preview_pod) && code_extracted(preview_pod),
        code_fetched: has_status(preview_pod) && code_fetched(preview_pod)
      },
      provisioning: %{
        docker: ready(docker_pod(pods), "docker")
      }
    }
  end

  @doc """
  Fetch the docker pod from a list of pods
  """
  def docker_pod(pods) do
    pods
    |> Enum.find(fn pod ->
      app_label(pod) == "docker"
    end)
  end

  @doc """
  Fetch the docker pod from a list of pods
  """
  def preview_pod(pods) do
    pods
    |> Enum.find(fn pod ->
      app_label(pod) == "preview"
    end)
  end

  # single

  @doc """
  Returns the name of the pod
  """
  def name(pod) do
    pod
    |> Map.get("metadata")
    |> Map.get("name")
  end

  @doc """
  Returns the name of the pod
  """
  def app_label(pod) do
    pod
    |> Map.get("metadata")
    |> Map.get("labels")
    |> Map.get("app")
  end

  def status(pod) do
    pod
    |> Map.get("status")
  end

  def has_status(pod) do
    status(pod) != nil
  end

  def phase(pod) do
    pod
    |> status
    |> Map.get("phase")
  end

  def container_statuses(pod) do
    pod
    |> status
    |> Map.get("containerStatuses")
  end

  def init_container_statuses(pod) do
    pod
    |> status
    |> Map.get("initContainerStatuses")
  end

  def container_status(pod, name) do
    pod
    |> container_statuses
    |> Enum.find(fn container_status ->
      container_status["name"] == name
    end)
  end

  def init_container_status(pod, name) do
    pod
    |> init_container_statuses
    |> Enum.find(fn init_container_status ->
      init_container_status["name"] == name
    end)
  end

  @doc """
  Returns the updated_at component of a resource result
  """
  def restart_count(pod) do
    case phase(pod) do
      "Running" ->
        pod
        |> container_status(Previews.KubernetesV2.Templates.Deployment.name())
        |> Map.get("restartCount")

      _ ->
        0
    end
  end

  @doc """
  Returns the Kubernetes ready status for the preview containers status.
  """
  def ready(pod, name) do
    case phase(pod) do
      "Running" ->
        pod
        |> container_status(name)
        |> Map.get("ready")

      _ ->
        false
    end
  end

  @doc """
  Has the preview container completed the download code init container task?
  """
  def code_fetched(pod) do
    case phase(pod) do
      "Running" ->
        "terminated" ==
          pod
          |> init_container_status("fetch-code")
          |> Map.get("state")
          |> Map.keys()
          |> List.first()

      _ ->
        false
    end
  end

  @doc """
  Has the preview container completed the download code init container task?
  """
  def code_extracted(pod) do
    case phase(pod) do
      "Running" ->
        "terminated" ==
          pod
          |> init_container_status("extract-code")
          |> Map.get("state")
          |> Map.keys()
          |> List.first()

      _ ->
        false
    end
  end
end
