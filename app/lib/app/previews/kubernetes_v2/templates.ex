defmodule App.Previews.KubernetesV2.Templates do
  alias App.{
    KubernetesV2,
    Previews
  }

  @doc """
  Provides an suitable name for a kubernetes namespace
  """
  @spec namespace(%Previews.Preview{}) :: String.t()
  def namespace(preview) do
    App.KubernetesV2.kid(preview.id)
  end

  @doc """
  The custom label used on resources created by stagehand
  """
  @spec label(%Previews.Preview{}) :: map
  def label(preview) do
    %{
      "preview_id" => preview.id
    }
  end

  @doc """
  Return a port_name for this port object
  TODO: Move this to a port module
  """
  @spec port_name(%Previews.Preview{}, map()) :: String.t()
  def port_name(preview, %{"name" => name, "value" => value}) do
    "#{KubernetesV2.kid("#{preview.id}#{name}#{value}")}"
  end

  @doc """
  Return a full url to this port
  TODO: Move this to a port module
  """
  @spec port_url(%Previews.Preview{}, map(), String.t()) :: String.t()
  def port_url(preview, port, preview_url) do
    port_name(preview, port)
    |> Previews.KubernetesV2.preview_url(preview_url)
  end
end
