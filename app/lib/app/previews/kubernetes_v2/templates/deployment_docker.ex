defmodule App.Previews.KubernetesV2.Templates.DeploymentDocker do
  @moduledoc """
  Defines a deployment of Docker used for this application to run.
  """

  alias App.{
    Previews
  }

  @doc """
  Name for this resource
  """
  @spec name() :: String.t()
  def name, do: "docker"

  @doc """
  Operation to determine if the resource exists
  """
  @spec get({%Previews.Preview{}}) :: K8s.Operation.t()
  def get({preview}) do
    K8s.Client.get("apps/v1", :deployment,
      namespace: Previews.KubernetesV2.Templates.namespace(preview),
      name: name()
    )
  end

  @doc """
  Create entry ports for the previews
  """
  @spec ports({%Previews.Preview{}}) :: nonempty_list(map())
  def ports({%{configuration: %{"ports" => ports}} = preview}) do
    k8s_ports =
      ports
      |> Enum.map(fn port ->
        %{
          "containerPort" => port["value"],
          "name" => Previews.KubernetesV2.Templates.port_name(preview, port)
        }
      end)

    k8s_ports ++
      [
        %{
          "containerPort" => 2375,
          "name" => "docker"
        }
      ]
  end

  @doc """
  Define the template for the resource
  """
  @spec template({%Previews.Preview{}}) :: map()
  def template({preview} = args) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{
        "name" => name(),
        "namespace" => Previews.KubernetesV2.Templates.namespace(preview)
      },
      "spec" => %{
        "replicas" => 1,
        "selector" => %{
          "matchLabels" => %{
            "app" => name()
          }
        },
        "template" => %{
          "metadata" => %{
            "labels" => %{
              "app" => name()
            }
          },
          "spec" => %{
            "containers" => [
              %{
                "image" => "docker:18.09-dind",
                "livenessProbe" => %{
                  "tcpSocket" => %{
                    "initialDelaySeconds" => 5,
                    "periodSeconds" => 5,
                    "port" => 2375
                  }
                },
                "name" => name(),
                "ports" => ports(args),
                "readinessProbe" => %{
                  "tcpSocket" => %{
                    "initialDelaySeconds" => 5,
                    "periodSeconds" => 5,
                    "port" => 2375
                  }
                },
                "securityContext" => %{
                  "privileged" => true
                },
                "volumeMounts" => [
                  %{
                    "name" => "docker-graph-storage",
                    "mountPath" => "/var/lib/docker"
                  }
                ]
              }
            ],
            "volumes" => [
              %{
                "name" => "docker-graph-storage",
                "emptyDir" => %{}
              }
            ]
          }
        }
      }
    }
  end
end
