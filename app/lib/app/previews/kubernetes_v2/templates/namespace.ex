defmodule App.Previews.KubernetesV2.Templates.Namespace do
  @moduledoc """
  Defines a namespace that previews related to the project will run in.
  """

  alias App.{
    KubernetesV2,
    Previews
  }

  @doc """
  Get all namespaces in the system
  TODO: Should move to kubernetes folder, this does not require a preview
  """
  @spec all() :: K8s.Operation.t()
  def all do
    K8s.Client.list("v1", :namespace)
  end

  @doc """
  Operation to determine if the resource exists
  """
  @spec get({%Previews.Preview{}}) :: K8s.Operation.t()
  def get({preview}) do
    K8s.Client.get("v1", :namespace, name: Previews.KubernetesV2.Templates.namespace(preview))
  end

  @doc """
  Operation to determine if the resource exists
  """
  @spec patch({%Previews.Preview{}}) :: K8s.Operation.t()
  def patch({preview}) do
    K8s.Client.patch(template({preview}))
  end

  @doc """
  Define the template for the resource
  """
  @spec template({%Previews.Preview{}}) :: map()
  def template({preview}) do
    %{
      "apiVersion" => "v1",
      "kind" => "Namespace",
      "metadata" => %{
        "name" => Previews.KubernetesV2.Templates.namespace(preview),
        "labels" =>
          Map.merge(
            KubernetesV2.Templates.label(),
            Previews.KubernetesV2.Templates.label(preview)
          ),
        "annotations" => KubernetesV2.Templates.Annotation.get()
      }
    }
  end
end
