defmodule App.Previews.Config do
  @moduledoc """
  Used for interacting with the configuration object given to us when starting a preview.
  """

  alias App.{
    Previews
  }

  @doc """
  The default configuration, this is merged with the configration given to use by the client when creating a preview.
  """
  @spec default() :: map()
  def default do
    %{
      "command" => "./stagehand.sh",
      "env" => [],
      "feedback" => %{
        "description" => "View in Stagehand",
        "key" => "stagehand",
        "name" => "Stagehand Preview"
      },
      "inactivity_timeout_seconds" => 7200,
      "ports" => [
        %{
          "name" => "App",
          "value" => 80
        }
      ],
      "version" => 1
    }
  end

  @doc """
  The default configuration, this is merged with the configration given to use by the client when creating a preview.
  """
  @spec inactivity_timeout_seconds(%Previews.Preview{}) :: integer()
  def inactivity_timeout_seconds(preview) do
    preview.configuration["inactivity_timeout_seconds"] || 300
  end
end
