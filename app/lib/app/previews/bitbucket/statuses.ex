defmodule App.Previews.Bitbucket.Statuses do
  alias App.{
    Previews,
    ProviderAuthorisations
  }

  def create(bitbucket_preview, state, feedback) do
    provider_authorisation = Previews.ProviderAuthorisations.get(bitbucket_preview)

    state =
      case state do
        :ready -> "SUCCESSFUL"
      end

    ProviderAuthorisations.Bitbucket.Statuses.create(
      provider_authorisation,
      bitbucket_preview.username,
      bitbucket_preview.repo_slug,
      bitbucket_preview.sha,
      %{
        description: feedback["description"],
        key: feedback["key"],
        name: feedback["name"],
        state: state,
        url:
          AppWeb.Router.Helpers.preview_url(AppWeb.Endpoint, :show, %{
            id: bitbucket_preview.id
          })
      }
    )
  end
end
