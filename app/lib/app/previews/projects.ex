defmodule App.Previews.Projects do
  alias App.{
    Projects
  }

  @doc """
  Gets the project for this preview
  """
  def get(%{project_id: project_id}) do
    Projects.get(project_id)
  end
end
