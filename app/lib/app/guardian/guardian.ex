defmodule App.Guardian do
  use Guardian, otp_app: :stagehand

  def subject_for_token(resource, _claims) do
    sub = to_string(resource.id)
    {:ok, sub}
  end

  def resource_from_claims(claims) do
    id = claims["sub"]
    resource = App.Users.get(id)
    {:ok, resource}
  end
end
