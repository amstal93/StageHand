defmodule App.KubernetesV2.Monitor do
  alias App.{KubernetesV2, Previews}
  require Logger
  use GenServer

  @requeue_time Application.get_env(:stagehand, :kubernetes_resource_poll_interval)
  @kubernetes_v2 Application.get_env(:stagehand, :kubernetes_v2)

  def start_link do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(state) do
    work()
    queue()
    {:ok, state}
  end

  def handle_info(:work, state) do
    work()
    queue()
    {:noreply, state}
  end

  def work do
    Logger.info("Scanning for orphaned previews")

    result =
      Previews.KubernetesV2.Templates.Namespace.all()
      |> @kubernetes_v2.run(options())

    case result do
      {:ok, response} ->
        items = response["items"]

        Logger.debug("Found #{length(items)} running preview(s)")

        items
        |> Enum.each(fn item ->
          preview = Previews.get(item["metadata"]["labels"]["preview_id"])

          Logger.info("Checking preview: #{preview.id}")

          if Previews.DynamicSupervisor.Monitor.missing?(preview) do
            Logger.warn("Orphaned preview found, relaunching...")
            Previews.launch(preview)
          end

          if Previews.DynamicSupervisor.Monitor.incomplete?(preview) do
            Logger.warn("Incomplete preview found, terminating preview")
            Previews.terminate(preview)
          end

          Logger.info("Finished preview check for: #{preview.id}")
        end)

      {:error, response} ->
        Logger.warn("Error in monitor scan: #{inspect(response)}")
    end

    Logger.info("Orphaned preview scan completed")
  end

  def queue do
    Process.send_after(self(), :work, @requeue_time)
  end

  def options() do
    [params: %{"labelSelector" => URI.encode_query(KubernetesV2.Templates.label())}]
  end
end
