defmodule App.KubernetesV2.Templates.Annotation do
  @moduledoc """
  Store additional unstructured data on resources (currently nothing)
  """

  @key "stagehand"

  @doc """
  Produces an annotation suitable for storing on preview level Kubernetes resources to reidentify them back to Stagehand
  """
  @spec get() :: map
  def get do
    value = Poison.encode!(%{})

    %{
      @key => value
    }
  end

  @doc """
  Parse an annotation produced by the get function
  """
  @spec parse(map) :: map
  def parse(resource) do
    resource
    |> Map.get("metadata")
    |> Map.get("annotations")
    |> Map.get(@key)
    |> Poison.decode!()
  end
end
