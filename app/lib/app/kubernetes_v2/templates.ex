defmodule App.KubernetesV2.Templates do
  @moduledoc """
  Functions generic to all template operations are located here
  """

  @doc """
  Label used to mark this is a stagehand resource
  """
  @spec label() :: String.t()
  def label do
    %{"stagehand" => "true"}
  end
end
