defmodule App.Repo.Migrations.CreateOrganisations do
  use Ecto.Migration

  def change do
    create table(:organisations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false
      add :access_token, :uuid, null: false, default: fragment("uuid_generate_v4()")
      timestamps()
    end

    create unique_index(:organisations, [:access_token])
  end
end
