defmodule App.Repo.Migrations.CreateProviderConnections  do
  use Ecto.Migration

  def change do
    create table(:provider_connections, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :organisation_id, references(:organisations, on_delete: :delete_all, type: :binary_id), null: false
      add :provider_id, references(:providers, on_delete: :delete_all, type: :binary_id), null: false
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false
      timestamps()
    end

    create unique_index(:provider_connections, [:organisation_id])
    create unique_index(:provider_connections, [:organisation_id, :user_id])
    create unique_index(:provider_connections, [:organisation_id, :provider_id])
  end
end
