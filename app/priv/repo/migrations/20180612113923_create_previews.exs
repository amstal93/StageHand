defmodule App.Repo.Migrations.CreatePreviews do
  use Ecto.Migration

  def change do
    create table(:previews, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :configuration, :map, null: false, default: %{}
      add :organisation_id, references(:organisations, on_delete: :delete_all, type: :binary_id), null: false
      add :provider_id, references(:providers, on_delete: :delete_all, type: :binary_id), null: false
      add :repo_slug, :string, null: false
      add :sha, :string, null: false
      add :username, :string, null: false
      add :version, :integer, null: false, default: 1
      timestamps()
    end

    create unique_index(:previews, [
      :organisation_id,
      :provider_id,
      :repo_slug,
      :sha, :username
    ])

    create constraint(:previews, "positive_version_check", check: "version >= 1")
  end
end
