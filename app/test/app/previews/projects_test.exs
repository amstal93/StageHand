defmodule App.Previews.ProjectsTest do
  alias App.{Previews.Projects}
  import App.Fixtures
  use App.DataCase, async: true

  setup do
    preview = insert(:preview)

    %{preview: preview}
  end

  test "get/1", %{preview: preview} do
    assert Projects.get(preview).id == preview.project_id
  end
end
