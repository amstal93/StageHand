defmodule App.Previews.ProviderAuthorisationsTest do
  alias App.{Previews.ProviderAuthorisations}
  import App.Fixtures
  use App.DataCase, async: false

  setup do
    provider_bitbucket = insert(:provider, name: "bitbucket")
    provider_github = insert(:provider, name: "github")

    project = insert(:project)
    user = insert(:user)

    provider_authorisation_bitbucket =
      insert(:provider_authorisation,
        user: user,
        provider: provider_bitbucket,
        expires_at: 1_654_724_864
      )

    provider_authorisation_github =
      insert(:provider_authorisation, user: user, provider: provider_github)

    insert(:provider_connection, project: project, provider: provider_bitbucket, user: user)
    insert(:provider_connection, project: project, provider: provider_github, user: user)

    preview_bitbucket = insert(:preview, project: project, provider: provider_bitbucket)

    preview_github =
      insert(:preview,
        project: project,
        provider: provider_github,
        id: "29e41d9a-4ca9-484b-82c1-df01dff3ab41"
      )

    %{
      preview_bitbucket: preview_bitbucket,
      preview_github: preview_github,
      provider_authorisation_bitbucket: provider_authorisation_bitbucket,
      provider_authorisation_github: provider_authorisation_github
    }
  end

  describe "get/1" do
    test "with bitbucket", %{
      preview_bitbucket: preview_bitbucket,
      provider_authorisation_bitbucket: provider_authorisation_bitbucket
    } do
      assert provider_authorisation_bitbucket.id ==
               ProviderAuthorisations.get(preview_bitbucket).id
    end

    test "with github", %{
      preview_github: preview_github,
      provider_authorisation_github: provider_authorisation_github
    } do
      assert provider_authorisation_github.id == ProviderAuthorisations.get(preview_github).id
    end
  end
end
