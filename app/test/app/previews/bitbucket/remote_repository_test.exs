defmodule App.Previews.Bitbucket.RemoteRepositoryTest do
  alias App.{Previews.Bitbucket.RemoteRepository}
  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)

    provider_authorisation =
      insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)

    insert(:provider_connection, project: project, provider: provider, user: user)
    bitbucket_preview = insert(:preview, project: project, provider: provider)

    %{
      bitbucket_preview: bitbucket_preview,
      provider_authorisation: provider_authorisation
    }
  end

  test "download_url/1", %{
    bitbucket_preview: bitbucket_preview
  } do
    assert RemoteRepository.download_url(bitbucket_preview) ==
             "https://bitbucket.org/username/repo_slug/get/sha.tar.gz"
  end

  test "auth_header/1", %{
    bitbucket_preview: bitbucket_preview,
    provider_authorisation: provider_authorisation
  } do
    assert RemoteRepository.auth_header(bitbucket_preview) ==
             "Authorization: Bearer #{provider_authorisation.token}"
  end
end
