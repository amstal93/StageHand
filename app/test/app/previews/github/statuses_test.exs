defmodule App.Previews.Github.StatusesTest do
  alias App.{
    Previews.Github.Statuses,
    Mocks.Http
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup :verify_on_exit!

  test "status/1" do
    provider = insert(:provider, name: "github")
    project = insert(:project, access_token: "29e41d9a-4ca9-484b-82c1-df01dff3ab40")

    provider_authorisation = insert(:provider_authorisation, provider: provider)

    insert(:provider_connection,
      provider: provider,
      user: provider_authorisation.user,
      project: project
    )

    preview =
      insert(:preview, %{
        provider: provider,
        project: project,
        repo_slug: "repo_slug",
        sha: "sha",
        username: "username"
      })

    feedback = %{
      "key" => "key",
      "name" => "Custom Title"
    }

    App.HTTPoisonMock
    |> expect(
      :request,
      Http.Github.create_status(
        %{
          username: preview.username,
          repo_slug: preview.repo_slug,
          sha: preview.sha,
          feedback: feedback
        },
        preview
      )
    )

    Statuses.create(preview, :ready, feedback)
  end
end
