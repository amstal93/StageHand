defmodule App.Previews.KubernetesV2.TemplatesTest do
  alias App.{
    Previews.KubernetesV2.Templates
  }

  import App.Fixtures

  use App.DataCase, async: true

  setup do
    Mox.verify_on_exit!()
    port = %{"name" => "MyPort", "value" => 80}
    preview = insert(:preview)
    %{port: port, preview: preview}
  end

  test "namespace/1", %{preview: preview} do
    assert Templates.namespace(preview) == "id9f12b94792201"
  end

  test "label/1", %{preview: preview} do
    assert Templates.label(preview) == %{
             "preview_id" => preview.id
           }
  end

  test "port_name/1", %{preview: preview, port: port} do
    assert Templates.port_name(preview, port) ==
             "idb6c85f20b47a4"
  end

  test "port_url/1", %{preview: preview, port: port} do
    assert Templates.port_url(preview, port, "http://stagehand.home:32568") ==
             "http://idb6c85f20b47a4.stagehand.home:32568"
  end
end
