defmodule App.Previews.KubernetesV2.Templates.PodTest do
  alias App.{
    Mocks.KubernetesV2.Operation,
    Previews.KubernetesV2.Templates.Pod
  }

  import App.Fixtures
  use App.DataCase, async: true

  setup do
    preview = insert(:preview)
    %{preview: preview}
  end

  describe "all/2" do
    test "docker pods", %{preview: preview} do
      assert Pod.all(preview, "docker") ==
               Operation.Pod.list_docker(preview)
    end

    test "preview pods", %{preview: preview} do
      assert Pod.all(preview, "preview") ==
               Operation.Pod.list_preview(preview)
    end
  end
end
