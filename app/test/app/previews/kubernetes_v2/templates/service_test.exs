defmodule App.Previews.KubernetesV2.Templates.ServiceTest do
  alias App.{
    Mocks.KubernetesV2.Operation,
    Previews.KubernetesV2.Templates.Service
  }

  import App.Fixtures
  use App.DataCase, async: true

  setup do
    preview = insert(:preview)

    port = %{
      "name" => "App",
      "value" => 80
    }

    %{preview: preview, port: port}
  end

  test "name/1", %{preview: preview, port: port} do
    assert Service.name({preview, port}) == "id9f12b94792201-id39da6a4e7d8ae"
  end

  test "get/1", %{preview: preview, port: port} do
    assert Service.get({preview, port}) ==
             Operation.Service.get(preview, port)
  end

  test "port_host_regex/1", %{preview: preview, port: port} do
    assert Service.port_host_regex(preview, port) == ".*id39da6a4e7d8ae.*"
  end

  test "template/1", %{preview: preview, port: port} do
    assert Service.template({preview, port}) == %{
             "apiVersion" => "v1",
             "kind" => "Service",
             "spec" => %{
               "ports" => [
                 %{"name" => "id39da6a4e7d8ae", "port" => 80, "targetPort" => "id39da6a4e7d8ae"}
               ],
               "selector" => %{"app" => "docker"}
             },
             "metadata" => %{
               "name" => "id9f12b94792201-id39da6a4e7d8ae",
               "namespace" => "id9f12b94792201",
               "annotations" => %{
                 "getambassador.io/config" =>
                   "---\napiVersion: \"ambassador/v1\"\nhost: \".*id39da6a4e7d8ae.*\"\nhost_regex: true\nkind: \"Mapping\"\nname: \"id9f12b94792201-id39da6a4e7d8ae\"\nprefix: \"/\"\nservice: \"id9f12b94792201-id39da6a4e7d8ae.id9f12b94792201\"\ntimeout_ms: 60000\nconnect_timeout_ms: 60000\nuse_websocket: true\nremove_response_headers:\n  - x-frame-options\n"
               }
             }
           }
  end
end
