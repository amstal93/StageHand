defmodule App.KubernetesV2.Templates.DeploymentDockerTest do
  alias App.{
    Mocks.KubernetesV2.Operation,
    Previews.KubernetesV2.Templates.DeploymentDocker
  }

  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider = insert(:provider, name: "bitbucket")
    project = insert(:project)

    provider_authorisation =
      insert(:provider_authorisation, provider: provider, expires_at: 1_654_724_864, token: "1")

    insert(:provider_connection,
      provider: provider,
      user: provider_authorisation.user,
      project: project
    )

    preview =
      insert(:preview, %{
        provider: provider,
        project: project,
        configuration: %{
          "env" => [
            %{
              "name" => "APP_ENV",
              "value" => "local"
            }
          ],
          "ports" => [
            %{
              "name" => "App",
              "value" => 80
            },
            %{
              "name" => "Storybook",
              "value" => 8081
            }
          ]
        }
      })

    %{preview: preview}
  end

  test "name/0" do
    assert DeploymentDocker.name() == "docker"
  end

  test "get/1", %{preview: preview} do
    assert DeploymentDocker.get({preview}) ==
             Operation.DeploymentDocker.get(preview)
  end

  # test "list/1", %{preview: preview} do
  #  assert DeploymentDocker.list({preview}) ==
  #           Operation.DeploymentDocker.list(preview)
  # end

  test "ports/1", %{preview: preview} do
    assert DeploymentDocker.ports({preview}) == [
             %{"containerPort" => 80, "name" => "id39da6a4e7d8ae"},
             %{"containerPort" => 8081, "name" => "id0e3ba36e89c70"},
             %{"containerPort" => 2375, "name" => "docker"}
           ]
  end

  test "template/1", %{preview: preview} do
    assert DeploymentDocker.template({preview}) == %{
             "apiVersion" => "apps/v1",
             "kind" => "Deployment",
             "metadata" => %{"name" => "docker", "namespace" => "id9f12b94792201"},
             "spec" => %{
               "replicas" => 1,
               "selector" => %{"matchLabels" => %{"app" => "docker"}},
               "template" => %{
                 "metadata" => %{"labels" => %{"app" => "docker"}},
                 "spec" => %{
                   "volumes" => [%{"emptyDir" => %{}, "name" => "docker-graph-storage"}],
                   "containers" => [
                     %{
                       "image" => "docker:18.09-dind",
                       "livenessProbe" => %{
                         "tcpSocket" => %{
                           "initialDelaySeconds" => 5,
                           "periodSeconds" => 5,
                           "port" => 2375
                         }
                       },
                       "name" => "docker",
                       "readinessProbe" => %{
                         "tcpSocket" => %{
                           "initialDelaySeconds" => 5,
                           "periodSeconds" => 5,
                           "port" => 2375
                         }
                       },
                       "securityContext" => %{"privileged" => true},
                       "volumeMounts" => [
                         %{"mountPath" => "/var/lib/docker", "name" => "docker-graph-storage"}
                       ],
                       "ports" => [
                         %{"containerPort" => 80, "name" => "id39da6a4e7d8ae"},
                         %{"containerPort" => 8081, "name" => "id0e3ba36e89c70"},
                         %{"containerPort" => 2375, "name" => "docker"}
                       ]
                     }
                   ]
                 }
               }
             }
           }
  end
end
