defmodule App.Previews.DynamicSupervisor.Monitor.HealthTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews,
    Previews.DynamicSupervisor.Monitor.Health
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: false

  setup :verify_on_exit!
  setup :set_mox_global

  setup do
    preview = insert(:preview)

    %{
      preview: preview
    }
  end

  test "name/1", %{preview: preview} do
    assert Health.name(preview) == {:monitor_health, "id9f12b94792201"}
  end

  test "via_name/1", %{preview: preview} do
    assert Health.via_name(preview) ==
             {:via, Registry, {:previews, {:monitor_health, "id9f12b94792201"}}}
  end

  test "start_link/1", %{preview: preview} do
    assert {:ok, pid} = Health.start_link([preview])
    assert is_pid(pid)
    GenServer.stop(pid)
    refute Process.alive?(pid)
  end

  test "init/1", %{preview: preview} do
    assert {:ok, state} = Health.init([preview])
    assert state[:preview] == preview
    refute_received :work
    :timer.sleep(5000)
    assert_received :work
  end

  test "whereis/1", %{preview: preview} do
    assert nil == Health.whereis(preview)
  end

  describe "handle_info/2" do
    test "reschedules health check when all pass", %{
      preview: preview
    } do
      App.UtilsMock

      # last_viewed_at startup time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      # check against current time
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      # check under total run time allowed
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      App.KubernetesV2Mock
      |> expect(:async, Expectation.read_all(preview, Response.read_all(preview)))
      |> expect(:run, Expectation.Pod.list_preview(preview, Response.Pod.list_preview(preview)))
      |> expect(:run, Expectation.Namespace.get(preview, Response.Namespace.get(preview)))

      {:ok, state_pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])
      {:ok, pid} = Previews.DynamicSupervisor.start_child(preview)
      Health.handle_info(:work, %{preview: preview})
      assert Process.alive?(pid)
      :timer.sleep(5000)
      assert_received :work
      Previews.DynamicSupervisor.terminate_child(preview)
      refute Process.alive?(pid)
      GenServer.stop(state_pid)
    end

    test "stops when deployment unhealthy", %{
      preview: preview
    } do
      # last_viewed_at startup time
      App.UtilsMock
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2016-07-01 11:03:58.970155Z")
        result
      end)

      App.KubernetesV2Mock
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))
      |> expect(:run, Expectation.Namespace.delete(preview, Response.Namespace.delete(preview)))

      {:ok, state_pid} = Previews.DynamicSupervisor.Monitor.State.start_link([preview])
      {:ok, pid} = Previews.DynamicSupervisor.start_child(preview)
      assert Process.alive?(pid) == true
      Health.handle_info(:work, %{preview: preview})
      refute Process.alive?(pid)
      GenServer.stop(state_pid)
    end
  end
end
