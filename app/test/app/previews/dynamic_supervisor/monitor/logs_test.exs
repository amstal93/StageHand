defmodule App.Previews.Monitor.LogsTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews.DynamicSupervisor.Monitor.Logs
  }

  import App.Fixtures
  import Mox
  import Phoenix.ChannelTest

  use App.DataCase

  setup :verify_on_exit!

  setup do
    preview = insert(:preview)
    %{preview: preview}
  end

  test "name/1", %{preview: preview} do
    assert Logs.name(preview) == {:monitor_logs, "id9f12b94792201"}
  end

  test "via_name/1", %{preview: preview} do
    assert Logs.via_name(preview) ==
             {:via, Registry, {:previews, {:monitor_logs, "id9f12b94792201"}}}
  end

  test "handle_frame/2", %{preview: preview} do
    message = "message"
    state = %{preview: preview}
    payload = {:text, message}

    AppWeb.Endpoint.subscribe(AppWeb.PreviewChannel.channel_name(preview))

    assert Logs.handle_frame(payload, state) == {:ok, state}
    assert_broadcast("log", %{data: ^message})
  end

  test "pod_name/1", %{preview: preview} do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Pod.list_preview(preview, Response.Pod.list_preview(preview)))

    assert "podname" == Logs.pod_name(preview)
  end

  test "exists?/1", %{preview: preview} do
    assert false == Logs.exists?(preview)
  end

  test "whereis/1", %{preview: preview} do
    assert nil == Logs.whereis(preview)
  end
end
