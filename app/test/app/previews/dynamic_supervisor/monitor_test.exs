defmodule App.Previews.DynamicSupervisor.MonitorTest do
  alias App.{
    Previews.DynamicSupervisor.Monitor
  }

  import App.Fixtures
  import Mox

  use App.DataCase, async: false

  setup :set_mox_global
  setup :verify_on_exit!

  setup do
    preview = insert(:preview)

    %{preview: preview}
  end

  test "name/1", %{preview: preview} do
    assert Monitor.name(preview) == {:monitor, "id9f12b94792201"}
  end

  test "via_name/1", %{preview: preview} do
    assert Monitor.via_name(preview) ==
             {:via, Registry, {:previews, {:monitor, "id9f12b94792201"}}}
  end

  test "start_link/1", %{preview: preview} do
    assert {:ok, pid} = Monitor.start_link([preview])
    assert is_pid(pid)
    DynamicSupervisor.stop(pid)
    :timer.sleep(50)
    assert Monitor.exists?(preview) == false
  end

  test "start_monitoring/1", %{preview: preview} do
    App.UtilsMock
    |> expect(:utc_now, fn ->
      {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
      result
    end)

    {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])
    assert {:ok, pid_health, pid_state, pid_status} = Monitor.start_monitoring(preview)
    assert is_pid(pid_health)
    assert is_pid(pid_state)
    assert is_pid(pid_status)

    DynamicSupervisor.stop(dynamic_supervisor_pid)
    :timer.sleep(50)
    assert Monitor.exists?(preview) == false
  end

  test "start_module/2", %{preview: preview} do
    module = App.Previews.DynamicSupervisor.Monitor.Health
    {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])
    assert {:ok, pid} = Monitor.start_module(module, preview)
    assert is_pid(pid)
    DynamicSupervisor.stop(dynamic_supervisor_pid)
    :timer.sleep(50)
    assert Monitor.exists?(preview) == false
  end

  test "terminate_module/2", %{preview: preview} do
    module = App.Previews.DynamicSupervisor.Monitor.Health
    assert {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])
    Monitor.start_module(module, preview)
    assert :ok == Monitor.terminate_module(module, preview)
    DynamicSupervisor.stop(dynamic_supervisor_pid)
    :timer.sleep(50)
    assert Monitor.exists?(preview) == false
  end

  test "whereis/1", %{preview: preview} do
    assert {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])
    assert pid = Monitor.whereis(preview)
    assert is_pid(pid)
    DynamicSupervisor.stop(pid)
    :timer.sleep(50)
    assert Monitor.exists?(preview) == false
  end

  describe "exists?/1" do
    test "with no process running", %{preview: preview} do
      assert Monitor.exists?(preview) == false
    end

    test "with process running", %{preview: preview} do
      assert Monitor.exists?(preview) == false
      {:ok, pid} = Monitor.start_link([preview])
      assert Monitor.exists?(preview) == true
      DynamicSupervisor.stop(pid)
      :timer.sleep(50)
      assert Monitor.exists?(preview) == false
    end
  end

  test "missing?/1", %{preview: preview} do
    assert Monitor.exists?(preview) == false
  end

  describe "complete?/1" do
    test "with no processes running", %{preview: preview} do
      assert Monitor.complete?(preview) == false
    end

    test "with an incomplete set of processes", %{preview: preview} do
      {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])

      Monitor.start_module(Monitor.Health, preview)
      assert Monitor.complete?(preview) == false

      DynamicSupervisor.stop(dynamic_supervisor_pid)
      :timer.sleep(50)
      assert Monitor.exists?(preview) == false
    end

    test "with an complete set of processes", %{preview: preview} do
      App.UtilsMock
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
        result
      end)

      {:ok, dynamic_supervisor_pid} = Monitor.start_link([preview])

      assert {:ok, _pid_health, _pid_state, _pid_status} = Monitor.start_monitoring(preview)

      assert Monitor.complete?(preview) == true

      DynamicSupervisor.stop(dynamic_supervisor_pid)
      :timer.sleep(50)
      assert Monitor.exists?(preview) == false
    end
  end
end
