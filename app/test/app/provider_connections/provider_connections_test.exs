defmodule App.ProviderRepositoriesTest do
  alias App.{ProviderConnections}
  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)

    %{provider: provider, project: project, user: user}
  end

  test "create/1", %{provider: provider, project: project, user: user} do
    attrs = %{
      project_id: project.id,
      provider_id: provider.id,
      user_id: user.id
    }

    assert {:ok, provider_connection} = ProviderConnections.create(attrs)
    assert provider_connection.project_id == project.id
    assert provider_connection.user_id == user.id
  end

  test "get_by/1", %{provider: provider, project: project} do
    inserted_provider_connection =
      insert(:provider_connection, provider: provider, project: project)

    assert provider_connection =
             ProviderConnections.get_by(
               project_id: inserted_provider_connection.project_id,
               user_id: inserted_provider_connection.user_id
             )

    assert inserted_provider_connection.id == provider_connection.id
  end

  describe "get_or_create_by/1" do
    test "when does not exist", %{provider: provider, project: project, user: user} do
      attrs = %{
        project_id: project.id,
        provider_id: provider.id,
        user_id: user.id
      }

      {:ok, provider_connection} = ProviderConnections.get_or_create_by(attrs)

      assert provider_connection.project_id == project.id
      assert provider_connection.provider_id == provider.id
      assert provider_connection.user_id == user.id
    end

    test "when does exist", %{provider: provider, project: project} do
      inserted_provider_connection =
        insert(:provider_connection, provider: provider, project: project)

      {:ok, provider_connection} =
        ProviderConnections.get_or_create_by(
          project_id: inserted_provider_connection.project_id,
          user_id: inserted_provider_connection.user_id
        )

      assert inserted_provider_connection.id == provider_connection.id
    end
  end

  describe "build_from_or_build_by/1" do
    test "when does not exist", %{provider: provider, project: project, user: user} do
      attrs = %{
        project_id: project.id,
        provider_id: provider.id,
        user_id: user.id
      }

      assert %Ecto.Changeset{changes: changes} = ProviderConnections.build_from_or_build_by(attrs)
      assert changes[:project_id] == project.id
      assert changes[:provider_id] == provider.id
      assert changes[:user_id] == user.id
    end

    test "when does exist", %{provider: provider, project: project, user: user} do
      inserted_provider_connection =
        insert(:provider_connection, provider: provider, project: project, user: user)

      assert %Ecto.Changeset{changes: changes} =
               ProviderConnections.build_from_or_build_by(%{
                 project_id: inserted_provider_connection.project_id,
                 provider_id: inserted_provider_connection.provider_id,
                 user_id: inserted_provider_connection.user_id
               })

      assert changes[:project_id] == inserted_provider_connection.project_id
      assert changes[:provider_id] == inserted_provider_connection.provider_id
      assert changes[:user_id] == inserted_provider_connection.user_id
    end
  end
end
