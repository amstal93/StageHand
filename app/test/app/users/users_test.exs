defmodule App.UsersTest do
  alias App.Users
  import App.Fixtures
  use App.DataCase, async: true

  @attrs %{
    email: "name@xample.com"
  }

  test "get/1" do
    user = insert(:user)
    assert Users.get(user.id) == user
  end

  test "create/1" do
    assert {:ok, user} = Users.create(@attrs)
    assert %Users.User{} = user
    assert user.email == @attrs[:email]
  end

  test "get_by/1" do
    %Users.User{email: email} = insert(:user, %{email: "test@example.com"})

    assert user = Users.get_by(email: email)

    assert %Users.User{} = user
  end

  test "update/2" do
    user = insert(:user)

    assert {:ok, %Users.User{} = user} = Users.update(user, @attrs)

    assert user.email == @attrs[:email]
  end
end
