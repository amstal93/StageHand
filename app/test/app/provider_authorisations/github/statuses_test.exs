defmodule App.ProviderAuthorisations.Github.StatusesTest do
  alias App.{ProviderAuthorisations.Github.Statuses}
  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  test "create/1" do
    description = "description"
    key = "key"
    name = "name"
    repo_slug = "repo_slug"
    sha = "sha"
    state = "state"
    url = "url"
    username = "username"

    provider_authorisation = insert(:provider_authorisation)

    App.HTTPoisonMock
    |> expect(:request, fn method, request_url, body, _headers ->
      assert method == :post

      assert request_url ==
               "https://api.github.com/repos/#{username}/#{repo_slug}/statuses/#{sha}"

      assert Poison.decode!(body) == %{
               "description" => description,
               "key" => key,
               "name" => name,
               "state" => state,
               "url" => url
             }

      {:ok, %HTTPoison.Response{}}
    end)

    Statuses.create(provider_authorisation, username, repo_slug, sha, %{
      "description" => description,
      "key" => key,
      "name" => name,
      "state" => state,
      "url" => url
    })
  end
end
