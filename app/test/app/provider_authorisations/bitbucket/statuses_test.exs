defmodule App.ProviderAuthorisations.Bitbucket.StatusesTest do
  alias App.{ProviderAuthorisations.Bitbucket.Statuses}
  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup do
    Mox.verify_on_exit!()
  end

  test "create/1" do
    description = "description"
    key = "key"
    name = "name"
    repo_slug = "repo_slug"
    sha = "sha"
    state = "state"
    url = "url"
    username = "username"

    provider_authorisation = insert(:provider_authorisation, expires_at: 1_654_724_864)

    App.HTTPoisonMock
    |> expect(:request, fn method, request_url, body, _headers ->
      assert method == :post

      assert request_url ==
               "https://api.bitbucket.org/2.0/repositories/#{username}/#{repo_slug}/commit/#{sha}/statuses/build"

      assert Poison.decode!(body) == %{
               "description" => description,
               "key" => key,
               "name" => name,
               "state" => state,
               "url" => url
             }

      {:ok, %HTTPoison.Response{}}
    end)

    Statuses.create(provider_authorisation, username, repo_slug, sha, %{
      "description" => description,
      "key" => key,
      "name" => name,
      "state" => state,
      "url" => url
    })
  end
end
