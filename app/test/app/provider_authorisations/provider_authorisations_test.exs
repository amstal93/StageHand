defmodule App.ProviderAuthorisationsTest do
  alias App.{ProviderAuthorisations, Repo}
  import App.Fixtures
  use App.DataCase

  setup do
    provider = insert(:provider)
    user = insert(:user)
    %{provider: provider, user: user}
  end

  test "build/1", %{provider: provider, user: user} do
    assert %Ecto.Changeset{} =
             ProviderAuthorisations.build(%{
               provider_id: provider.id,
               user_id: user.id
             })
  end

  test "create/1", %{provider: provider, user: user} do
    assert {:ok, provider_authorisation} =
             ProviderAuthorisations.create(%{
               expires: false,
               provider_id: provider.id,
               token: "1234",
               user_id: user.id
             })

    assert provider_authorisation.provider_id == provider.id
    assert provider_authorisation.user_id == user.id
  end

  test "update/2", %{provider: provider, user: user} do
    provider_authorisation = insert(:provider_authorisation, provider: provider)
    refute provider_authorisation.user_id == user.id

    assert {:ok, provider_authorisation} =
             ProviderAuthorisations.update(provider_authorisation, %{user_id: user.id})

    assert provider_authorisation.user_id == user.id
  end

  test "get/1", %{provider: provider} do
    provider_authorisation = insert(:provider_authorisation, provider: provider)

    assert provider_authorisation ==
             ProviderAuthorisations.get(provider_authorisation.id)
             |> Repo.preload(:user)
             |> Repo.preload(:provider)
  end

  test "get_by/1", %{provider: provider, user: user} do
    provider_authorisation = insert(:provider_authorisation, provider: provider, user: user)

    assert provider_authorisation ==
             ProviderAuthorisations.get_by(
               provider_id: provider_authorisation.provider.id,
               user_id: user.id
             )
             |> Repo.preload(:user)
             |> Repo.preload(:provider)
  end

  describe "get_or_create_by/1" do
    test "when does not exist", %{provider: provider, user: user} do
      {:ok, provider_authorisation} =
        ProviderAuthorisations.get_or_create_by(%{
          expires: true,
          provider_id: provider.id,
          token: "1234",
          user_id: user.id
        })

      assert provider_authorisation.provider_id == provider.id
      assert provider_authorisation.user_id == user.id
    end

    test "when does exist", %{provider: provider, user: user} do
      provider_authorisation = insert(:provider_authorisation, provider: provider, user: user)

      {:ok, returned_provider_authorisation} =
        ProviderAuthorisations.get_or_create_by(
          provider_id: provider.id,
          user_id: user.id
        )

      assert provider_authorisation ==
               returned_provider_authorisation
               |> Repo.preload(:user)
               |> Repo.preload(:provider)
    end
  end

  describe "expired?/1" do
    test "true when expired", %{provider: provider} do
      provider_authorisation =
        insert(:provider_authorisation, provider: provider, expires_at: 1_554_123_809)

      assert ProviderAuthorisations.expired?(provider_authorisation)
    end

    test "false when not expired", %{provider: provider} do
      provider_authorisation =
        insert(:provider_authorisation, provider: provider, expires_at: 1_664_123_809)

      refute ProviderAuthorisations.expired?(provider_authorisation)
    end

    test "false when cannot expire", %{provider: provider} do
      provider_authorisation = insert(:provider_authorisation, provider: provider, expires: false)

      refute ProviderAuthorisations.expired?(provider_authorisation)
    end
  end
end
