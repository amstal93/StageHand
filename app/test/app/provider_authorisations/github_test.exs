defmodule App.Github.ProviderAuthorisations.GithubTest do
  alias App.{ProviderAuthorisations.Github}
  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup do
    Mox.verify_on_exit!()
  end

  test "request/5" do
    provider_authorisation = insert(:provider_authorisation)

    App.HTTPoisonMock
    |> expect(:request, fn method, url, _body, _headers ->
      assert method == :get
      assert url == "https://api.github.com/example"
      {:ok, %HTTPoison.Response{}}
    end)

    assert Github.request(provider_authorisation, :get, "/example") ==
             {:ok, %HTTPoison.Response{}}
  end

  test "default_headers/1" do
    provider_authorisation = insert(:provider_authorisation)

    assert Github.default_headers(provider_authorisation) == [
             Accept: "application/vnd.github.v3+json",
             Authorization: "token #{provider_authorisation.token}",
             "Content-Type": "application/json"
           ]
  end
end
