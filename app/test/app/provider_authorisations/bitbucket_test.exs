defmodule App.Bitbucket.ProviderAuthorisations.BitbucketTest do
  alias App.{ProviderAuthorisations.Bitbucket}
  import App.Fixtures
  import Mox
  use App.DataCase, async: true

  setup do
    Mox.verify_on_exit!()
  end

  test "request/5" do
    provider_authorisation = insert(:provider_authorisation, expires_at: 1_654_724_864)

    App.HTTPoisonMock
    |> expect(:request, fn method, url, _body, _headers ->
      assert method == :get
      assert url == "https://api.bitbucket.org/example"
      {:ok, %HTTPoison.Response{}}
    end)

    assert Bitbucket.request(provider_authorisation, :get, "/example") ==
             {:ok, %HTTPoison.Response{}}
  end

  test "default_headers/1" do
    provider_authorisation = insert(:provider_authorisation)

    assert Bitbucket.default_headers(provider_authorisation) == [
             Authorization: "Bearer #{provider_authorisation.token}",
             "Content-Type": "application/json"
           ]
  end
end
