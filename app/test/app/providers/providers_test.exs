defmodule App.ProvidersTest do
  alias App.{Providers, Repo}
  import App.Fixtures
  use App.DataCase

  test "create!/1" do
    name = "github"

    assert %Providers.Provider{} =
             provider =
             Providers.create!(%{
               name: name
             })

    assert provider.name == name
  end

  test "get_by/1" do
    provider = insert(:provider)
    assert fetched_provider = Providers.get_by(name: provider.name)
    assert provider == fetched_provider
  end

  describe "get/1" do
    test "gets by id" do
      provider = insert(:provider)
      assert fetched_provider = Providers.get(provider.id)
      assert provider == fetched_provider
    end

    test "gets by bitbucket" do
      provider = insert(:provider, name: "bitbucket")
      assert fetched_provider = Providers.get(:bitbucket)
      assert provider == fetched_provider
    end

    test "gets by github" do
      provider = insert(:provider, name: "github")
      assert fetched_provider = Providers.get(:github)
      assert provider == fetched_provider
    end
  end

  describe "enabled?/1" do
    test "when bitbucket" do
      assert Providers.enabled?(:bitbucket) == true
    end

    test "when github" do
      assert Providers.enabled?(:github) == false
    end
  end
end
