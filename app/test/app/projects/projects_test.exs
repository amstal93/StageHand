defmodule App.ProjectsTest do
  alias App.Projects
  import App.Fixtures
  use App.DataCase, async: true

  test "create/1" do
    attrs = %{
      name: "Cool Co"
    }

    assert {:ok, project} = Projects.create(attrs)
    assert %Projects.Project{} = project
    assert project.name == attrs[:name]
  end

  test "get/1" do
    project = insert(:project)
    assert Projects.get(project.id).id == project.id
  end

  test "all/0" do
    inserted_project_1 = insert(:project)
    inserted_project_2 = insert(:project, id: "a94f065a-7ca5-431d-aa9c-b3074eec62ca")

    projects = Projects.all()
    [project_1, project_2] = projects

    assert length(projects) == 2
    assert inserted_project_1.id == project_1.id
    assert inserted_project_2.id == project_2.id
  end

  test "total/0" do
    insert(:project)
    insert(:project, id: "a94f065a-7ca5-431d-aa9c-b3074eec62ca")
    assert Projects.total() == 2
  end

  @tag :skip
  test "ensure_previews_monitored/0" do
    insert(:project)
    assert Projects.ensure_previews_monitored() == :ok
  end
end
