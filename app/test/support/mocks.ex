Mox.defmock(App.HTTPoisonMock, for: HTTPoison.Base)
Mox.defmock(App.KubernetesV2Mock, for: App.KubernetesV2)
Mox.defmock(App.UtilsMock, for: App.Utils)
