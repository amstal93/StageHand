defmodule App.Mocks.KubernetesV2.Response do
  def read_all(_preview) do
    [{:ok, %{}}, {:ok, %{}}, {:ok, %{}}, {:ok, %{}}, {:ok, %{}}]
  end

  def create_all(_preview) do
    [{:ok, %{}}, {:ok, %{}}, {:ok, %{}}, {:ok, %{}}, {:ok, %{}}]
  end

  def create_error_forbidden(_preview) do
    [
      {:ok, %{}},
      {:error, %HTTPoison.Response{status_code: 403}},
      {:ok, %{}},
      {:ok, %{}},
      {:ok, %{}}
    ]
  end

  def create_error_unknown(_preview) do
    [
      {:ok, %{}},
      {:error, %HTTPoison.Response{status_code: nil}},
      {:ok, %{}},
      {:ok, %{}},
      {:ok, %{}}
    ]
  end

  def read_all_missing(_preview) do
    [
      {:error, :not_found},
      {:error, :not_found},
      {:error, :not_found},
      {:error, :not_found},
      {:error, :not_found}
    ]
  end
end
