defmodule App.Mocks.KubernetesV2.Response.Namespace do
  def get(_preview) do
    {:ok,
     %{
       "apiVersion" => "v1",
       "kind" => "Namespace",
       "metadata" => %{
         "annotations" => %{
           "stagehand" => "{}"
         },
         "creationTimestamp" => "2019-09-15T08:14:43Z",
         "labels" => %{
           "preview_id" => "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
           "stagehand" => "true"
         },
         "name" => "id9f12b94792201"
       }
     }}
  end

  def get_expired(_preview) do
    {:ok,
     %{
       "apiVersion" => "v1",
       "kind" => "Namespace",
       "metadata" => %{
         "annotations" => %{
           "stagehand" => "{}"
         },
         "creationTimestamp" => "2014-09-15T08:14:43Z",
         "labels" => %{
           "preview_id" => "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
           "stagehand" => "true"
         },
         "name" => "id9f12b94792201"
       }
     }}
  end

  def delete(_preview) do
    {:ok, %{}}
  end
end
