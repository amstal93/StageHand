defmodule App.Mocks.KubernetesV2.Response.Pod do
  def docker(_preview) do
    %{
      "metadata" => %{
        "creationTimestamp" => "2016-07-01T11:03:58.970155Z",
        "labels" => %{
          "app" => "docker"
        },
        "name" => "podname"
      },
      "status" => %{
        "phase" => "Running",
        "containerStatuses" => [
          %{
            "name" => "docker",
            "restartCount" => 4,
            "ready" => true
          }
        ]
      }
    }
  end

  def preview(_preview) do
    %{
      "metadata" => %{
        "creationTimestamp" => "2016-07-01T11:03:58.970155Z",
        "labels" => %{
          "app" => "preview"
        },
        "name" => "podname"
      },
      "status" => %{
        "containerStatuses" => [
          %{
            "name" => "preview",
            "restartCount" => 4,
            "ready" => true
          }
        ],
        "phase" => "Running",
        "initContainerStatuses" => [
          %{
            "name" => "fetch-code",
            "state" => %{
              "terminated" => %{
                "reason" => "Completed"
              }
            }
          },
          %{
            "name" => "extract-code",
            "state" => %{
              "terminated" => %{
                "reason" => "Completed"
              }
            }
          }
        ]
      }
    }
  end

  def preview_exceeded(_preview) do
    %{
      "metadata" => %{
        "creationTimestamp" => "2016-07-01T11:03:58.970155Z",
        "labels" => %{
          "app" => "preview"
        },
        "name" => "podname"
      },
      "status" => %{
        "containerStatuses" => [
          %{
            "name" => "preview",
            "restartCount" => 40
          }
        ],
        "phase" => "Running"
      }
    }
  end

  def list(preview) do
    {:ok,
     %{
       "items" => [
         preview(preview),
         docker(preview)
       ]
     }}
  end

  def list_preview(preview) do
    {:ok,
     %{
       "items" => [
         preview(preview)
       ]
     }}
  end

  def list_preview_exceeded(preview) do
    {:ok,
     %{
       "items" => [
         preview_exceeded(preview)
       ]
     }}
  end
end
