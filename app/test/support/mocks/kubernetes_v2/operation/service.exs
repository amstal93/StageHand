defmodule App.Mocks.KubernetesV2.Operation.Service do
  def get(_preview, _port) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [namespace: "id9f12b94792201", name: "id9f12b94792201-id39da6a4e7d8ae"],
      verb: :get,
      api_version: "v1",
      name: :service
    }
  end

  def patch(preview, port) do
    %K8s.Operation{
      method: :patch,
      path_params: [namespace: "id9f12b94792201", name: "id9f12b94792201-id39da6a4e7d8ae"],
      verb: :patch,
      api_version: "v1",
      data: data(preview, port),
      name: "Service"
    }
  end

  def create(preview, port) do
    %K8s.Operation{
      method: :post,
      path_params: [namespace: "id9f12b94792201", name: "id9f12b94792201-id39da6a4e7d8ae"],
      verb: :create,
      api_version: "v1",
      data: data(preview, port),
      name: "Service"
    }
  end

  defp data(_preview, _port) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "annotations" => %{
          "getambassador.io/config" =>
            "---\napiVersion: \"ambassador/v1\"\nhost: \".*id39da6a4e7d8ae.*\"\nhost_regex: true\nkind: \"Mapping\"\nname: \"id9f12b94792201-id39da6a4e7d8ae\"\nprefix: \"/\"\nservice: \"id9f12b94792201-id39da6a4e7d8ae.id9f12b94792201\"\ntimeout_ms: 60000\nconnect_timeout_ms: 60000\nuse_websocket: true\nremove_response_headers:\n  - x-frame-options\n"
        },
        "name" => "id9f12b94792201-id39da6a4e7d8ae",
        "namespace" => "id9f12b94792201"
      },
      "spec" => %{
        "ports" => [
          %{"name" => "id39da6a4e7d8ae", "port" => 80, "targetPort" => "id39da6a4e7d8ae"}
        ],
        "selector" => %{"app" => "docker"}
      }
    }
  end
end
