defmodule App.Mocks.KubernetesV2.Operation.Namespace do
  def list() do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [],
      verb: :list,
      api_version: "v1",
      name: :namespace
    }
  end

  def get(_preview) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [name: "id9f12b94792201"],
      verb: :get,
      api_version: "v1",
      name: :namespace
    }
  end

  def delete(_preview) do
    %K8s.Operation{
      data: nil,
      method: :delete,
      path_params: [name: "id9f12b94792201"],
      verb: :delete,
      api_version: "v1",
      name: "Namespace"
    }
  end

  def patch(_preview) do
    %K8s.Operation{
      method: :patch,
      path_params: [name: "id9f12b94792201"],
      verb: :patch,
      api_version: "v1",
      data: %{
        "apiVersion" => "v1",
        "kind" => "Namespace",
        "metadata" => %{
          "annotations" => %{"stagehand" => "{}"},
          "labels" => %{
            "preview_id" => "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
            "stagehand" => "true"
          },
          "name" => "id9f12b94792201"
        }
      },
      name: "Namespace"
    }
  end

  def create(_preview) do
    %K8s.Operation{
      method: :post,
      path_params: [name: "id9f12b94792201"],
      verb: :create,
      api_version: "v1",
      data: %{
        "apiVersion" => "v1",
        "kind" => "Namespace",
        "metadata" => %{
          "annotations" => %{"stagehand" => "{}"},
          "labels" => %{
            "preview_id" => "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
            "stagehand" => "true"
          },
          "name" => "id9f12b94792201"
        }
      },
      name: "Namespace"
    }
  end
end
