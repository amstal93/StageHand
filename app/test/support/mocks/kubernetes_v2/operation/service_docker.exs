defmodule App.Mocks.KubernetesV2.Operation.ServiceDocker do
  def get(_preview) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :get,
      api_version: "v1",
      name: :service
    }
  end

  def patch(preview) do
    %K8s.Operation{
      method: :patch,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :patch,
      api_version: "v1",
      data: data(preview),
      name: "Service"
    }
  end

  def create(preview) do
    %K8s.Operation{
      method: :post,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :create,
      api_version: "v1",
      data: data(preview),
      name: "Service"
    }
  end

  defp data(_preview) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "name" => "docker",
        "namespace" => "id9f12b94792201"
      },
      "spec" => %{
        "selector" => %{
          "app" => "docker"
        },
        "ports" => [
          %{
            "port" => 2375,
            "targetPort" => 2375,
            "protocol" => "TCP"
          }
        ]
      }
    }
  end
end
