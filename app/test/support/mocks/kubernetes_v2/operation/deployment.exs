defmodule App.Mocks.KubernetesV2.Operation.Deployment do
  def get(_preview) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [namespace: "id9f12b94792201", name: "preview"],
      verb: :get,
      api_version: "apps/v1",
      name: :deployment
    }
  end

  def patch(preview) do
    %K8s.Operation{
      method: :patch,
      path_params: [namespace: "id9f12b94792201", name: "preview"],
      verb: :patch,
      api_version: "apps/v1",
      data: data(preview),
      name: "Deployment"
    }
  end

  def create(preview) do
    %K8s.Operation{
      method: :post,
      path_params: [namespace: "id9f12b94792201", name: "preview"],
      verb: :create,
      api_version: "apps/v1",
      data: data(preview),
      name: "Deployment"
    }
  end

  defp data(_preview) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{"name" => "preview", "namespace" => "id9f12b94792201"},
      "spec" => %{
        "replicas" => 1,
        "selector" => %{"matchLabels" => %{"app" => "preview"}},
        "template" => %{
          "metadata" => %{"labels" => %{"app" => "preview"}},
          "spec" => %{
            "containers" => [
              %{
                "command" => ["sh", "-c", "cd /repo-contents && sh"],
                "env" => [
                  %{"name" => "DOCKER_HOST", "value" => "tcp://docker:2375"},
                  %{
                    "name" => "PORT_80_HOST",
                    "value" => "http://id39da6a4e7d8ae.stagehand.home:32568"
                  },
                  %{"name" => "APP_ENV", "value" => "local"}
                ],
                "image" => "tmaier/docker-compose:19",
                "name" => "preview",
                "volumeMounts" => [%{"mountPath" => "/repo-contents", "name" => "repo-contents"}]
              }
            ],
            "initContainers" => [
              %{
                "command" => [
                  "curl",
                  "--location",
                  "--header",
                  "Authorization: Bearer 1",
                  "--output",
                  "/repo-download/archive.tar.gz",
                  "https://bitbucket.org/username/repo_slug/get/sha.tar.gz"
                ],
                "image" => "appropriate/curl:latest",
                "name" => "fetch-code",
                "volumeMounts" => [%{"mountPath" => "/repo-download", "name" => "repo-download"}]
              },
              %{
                "command" => [
                  "tar",
                  "--strip-components=1",
                  "--extract",
                  "--verbose",
                  "--file=/repo-download/archive.tar.gz",
                  "--directory",
                  "/repo-contents"
                ],
                "image" => "busybox:latest",
                "name" => "extract-code",
                "volumeMounts" => [
                  %{"mountPath" => "/repo-download", "name" => "repo-download"},
                  %{"mountPath" => "/repo-contents", "name" => "repo-contents"}
                ]
              }
            ],
            "volumes" => [
              %{"emptyDir" => %{}, "name" => "repo-download"},
              %{"emptyDir" => %{}, "name" => "repo-contents"}
            ]
          }
        }
      }
    }
  end
end
