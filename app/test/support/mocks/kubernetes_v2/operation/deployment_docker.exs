defmodule App.Mocks.KubernetesV2.Operation.DeploymentDocker do
  def get(_preview) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :get,
      api_version: "apps/v1",
      name: :deployment
    }
  end

  def list(_preview) do
    %K8s.Operation{
      data: nil,
      method: :get,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :list,
      api_version: "apps/v1",
      name: :deployment
    }
  end

  def patch(preview) do
    %K8s.Operation{
      method: :patch,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :patch,
      api_version: "apps/v1",
      data: data(preview),
      name: "Deployment"
    }
  end

  def create(preview) do
    %K8s.Operation{
      method: :post,
      path_params: [namespace: "id9f12b94792201", name: "docker"],
      verb: :create,
      api_version: "apps/v1",
      data: data(preview),
      name: "Deployment"
    }
  end

  defp data(_preview) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{
        "name" => "docker",
        "namespace" => "id9f12b94792201"
      },
      "spec" => %{
        "replicas" => 1,
        "selector" => %{
          "matchLabels" => %{
            "app" => "docker"
          }
        },
        "template" => %{
          "metadata" => %{
            "labels" => %{
              "app" => "docker"
            }
          },
          "spec" => %{
            "containers" => [
              %{
                "image" => "docker:18.09-dind",
                "livenessProbe" => %{
                  "tcpSocket" => %{
                    "initialDelaySeconds" => 5,
                    "periodSeconds" => 5,
                    "port" => 2375
                  }
                },
                "name" => "docker",
                "ports" => [
                  %{"containerPort" => 80, "name" => "id39da6a4e7d8ae"},
                  %{"containerPort" => 2375, "name" => "docker"}
                ],
                "readinessProbe" => %{
                  "tcpSocket" => %{
                    "initialDelaySeconds" => 5,
                    "periodSeconds" => 5,
                    "port" => 2375
                  }
                },
                "securityContext" => %{
                  "privileged" => true
                },
                "volumeMounts" => [
                  %{
                    "name" => "docker-graph-storage",
                    "mountPath" => "/var/lib/docker"
                  }
                ]
              }
            ],
            "volumes" => [
              %{
                "name" => "docker-graph-storage",
                "emptyDir" => %{}
              }
            ]
          }
        }
      }
    }
  end
end
