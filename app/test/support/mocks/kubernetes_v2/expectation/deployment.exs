defmodule App.Mocks.KubernetesV2.Expectation.Deployment do
  alias App.{
    Mocks.KubernetesV2.Operation
  }

  use ExUnit.Case, async: true

  def get(preview, response) do
    fn operation ->
      assert operation == Operation.Deployment.get(preview)
      response
    end
  end
end
