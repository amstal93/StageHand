defmodule AppWeb.Api.PreviewControllerTest do
  alias App.{
    Mocks.Http
  }

  import App.Fixtures
  import AppWeb.Router.Helpers
  import Mox
  use AppWeb.ConnCase, async: true

  setup do
    Mox.verify_on_exit!()

    provider = insert(:provider, name: "bitbucket")
    project = insert(:project)
    user = insert(:user)

    insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)
    insert(:provider_connection, project: project, provider: provider, user: user)

    %{
      project: project,
      repo_slug: "repo_slug",
      sha: "sha",
      username: "username"
    }
  end

  describe "POST /api/preview" do
    test "creates preview", %{
      conn: conn,
      project: project,
      repo_slug: repo_slug,
      sha: sha,
      username: username
    } do
      command = "command"

      env = [
        %{
          "name" => "name",
          "value" => "VALUE"
        }
      ]

      feedback = %{
        "feedback" => "feedback",
        "name" => "Custom Title",
        "description" => "All about it"
      }

      ports = [
        %{
          "name" => "App",
          "value" => 80
        },
        %{
          "name" => "Storybook",
          "value" => 8081
        }
      ]

      version = 2

      attrs = %{
        "access_token" => project.access_token,
        "command" => command,
        "env" => env,
        "feedback" => feedback,
        "ports" => ports,
        "repo_slug" => repo_slug,
        "sha" => sha,
        "username" => username,
        "version" => version
      }

      # Tested downstream in bitbucket_previews/commits_test.ex
      App.HTTPoisonMock
      |> expect(
        :request,
        Http.Bitbucket.create_status_build(%{
          username: username,
          repo_slug: repo_slug,
          sha: sha,
          feedback: feedback
        })
      )

      conn =
        conn
        |> post(
          preview_path(AppWeb.Endpoint, :create),
          attrs
        )

      assert %{
               "configuration" => %{
                 "command" => "command",
                 "env" => [%{"name" => "name", "value" => "VALUE"}],
                 "feedback" => %{
                   "description" => "All about it",
                   "feedback" => "feedback",
                   "name" => "Custom Title"
                 },
                 "inactivity_timeout_seconds" => 7200,
                 "ports" => [
                   %{"name" => "App", "value" => 80},
                   %{"name" => "Storybook", "value" => 8081}
                 ],
                 "repo_slug" => "repo_slug",
                 "sha" => "sha",
                 "username" => "username",
                 "version" => 2
               },
               "preview_url" => preview_url,
               "id" => _id
             } = json_response(conn, 201)

      assert preview_url =~ "/preview?id="
    end

    test "can omit defaults", %{
      conn: conn,
      project: project,
      repo_slug: repo_slug,
      sha: sha,
      username: username
    } do
      attrs = %{
        "access_token" => project.access_token,
        "repo_slug" => repo_slug,
        "sha" => sha,
        "username" => username
      }

      App.HTTPoisonMock
      |> expect(
        :request,
        Http.Bitbucket.create_status_build(%{
          username: username,
          repo_slug: repo_slug,
          sha: sha,
          feedback: %{
            "key" => "stagehand",
            "name" => "Stagehand Preview",
            "description" => "View in Stagehand"
          }
        })
      )

      conn =
        conn
        |> post(
          preview_path(AppWeb.Endpoint, :create),
          attrs
        )

      assert %{
               "configuration" => %{
                 "command" => "./stagehand.sh",
                 "env" => [],
                 "feedback" => %{
                   "description" => "View in Stagehand",
                   "key" => "stagehand",
                   "name" => "Stagehand Preview"
                 },
                 "inactivity_timeout_seconds" => 7200,
                 "ports" => [%{"name" => "App", "value" => 80}],
                 "repo_slug" => "repo_slug",
                 "sha" => "sha",
                 "username" => "username",
                 "version" => 1
               },
               "id" => _id,
               "preview_url" => preview_url
             } = json_response(conn, 201)

      assert preview_url =~ "/preview?id="
    end

    test "can create two previews on same commit", %{
      conn: conn,
      project: project,
      repo_slug: repo_slug,
      sha: sha,
      username: username
    } do
      attrs = %{
        "access_token" => project.access_token,
        "repo_slug" => repo_slug,
        "sha" => sha,
        "username" => username
      }

      App.HTTPoisonMock
      |> expect(
        :request,
        Http.Bitbucket.create_status_build(%{
          username: username,
          repo_slug: repo_slug,
          sha: sha,
          feedback: %{
            "key" => "stagehand",
            "name" => "Stagehand Preview",
            "description" => "View in Stagehand"
          }
        })
      )
      |> expect(
        :request,
        Http.Bitbucket.create_status_build(%{
          username: username,
          repo_slug: repo_slug,
          sha: sha,
          feedback: %{
            "key" => "stagehand",
            "name" => "Stagehand Preview",
            "description" => "View in Stagehand"
          }
        })
      )

      conn =
        conn
        |> post(
          preview_path(AppWeb.Endpoint, :create),
          attrs
        )

      assert %{
               "configuration" => %{
                 "command" => "./stagehand.sh",
                 "env" => [],
                 "feedback" => %{
                   "description" => "View in Stagehand",
                   "key" => "stagehand",
                   "name" => "Stagehand Preview"
                 },
                 "inactivity_timeout_seconds" => 7200,
                 "ports" => [%{"name" => "App", "value" => 80}],
                 "repo_slug" => "repo_slug",
                 "sha" => "sha",
                 "username" => "username",
                 "version" => 1
               },
               "id" => _id,
               "preview_url" => preview_url
             } = json_response(conn, 201)

      assert preview_url =~ "/preview?id="

      conn =
        conn
        |> post(
          preview_path(AppWeb.Endpoint, :create),
          attrs
        )

      assert %{
               "configuration" => %{
                 "command" => "./stagehand.sh",
                 "env" => [],
                 "feedback" => %{
                   "description" => "View in Stagehand",
                   "key" => "stagehand",
                   "name" => "Stagehand Preview"
                 },
                 "inactivity_timeout_seconds" => 7200,
                 "ports" => [%{"name" => "App", "value" => 80}],
                 "repo_slug" => "repo_slug",
                 "sha" => "sha",
                 "username" => "username",
                 "version" => 1
               },
               "id" => _id,
               "preview_url" => _preview_url
             } = json_response(conn, 201)

      assert preview_url =~ "/preview?id="
    end
  end
end
