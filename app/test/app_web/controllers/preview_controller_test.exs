defmodule AppWeb.PreviewControllerTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews
  }

  import App.Fixtures
  import AppWeb.Router.Helpers
  import Mox

  use AppWeb.ConnCase, async: false

  setup :set_mox_global
  setup :verify_on_exit!

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)
    insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)
    insert(:provider_connection, project: project, provider: provider, user: user)

    preview =
      insert(:preview,
        project: project,
        provider: provider
      )

    %{preview: preview, user: user}
  end

  describe "GET /preview" do
    test "launches preview when does not exist", %{conn: conn, preview: preview, user: user} do
      App.UtilsMock
      |> expect(:utc_now, fn ->
        {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
        result
      end)

      App.KubernetesV2Mock

      # Exists?
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      # Read before create/update operations
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      # Create assets
      |> expect(:async, Expectation.start(preview, Response.create_all(preview)))

      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> get(preview_path(AppWeb.Endpoint, :show), %{
          "id" => preview.id
        })

      assert conn.private[:phoenix_gon].assets[:channel_name] ==
               AppWeb.PreviewChannel.channel_name(preview)

      assert conn.private[:phoenix_gon].assets[:channel_token]

      assert conn.private[:phoenix_gon].assets[:description] == "An awesome preview"

      assert conn.private[:phoenix_gon].assets[:launch] == [:ok, :success]

      assert conn.private[:phoenix_gon].assets[:ports] == [
               %{
                 config: %{"name" => "App", "value" => 80},
                 id: "id39da6a4e7d8ae",
                 url: "http://id39da6a4e7d8ae.stagehand.home:32568"
               }
             ]

      assert conn.private[:phoenix_gon].assets[:preview] == %{
               provisioning: %{docker: false},
               readiness: [
                 %{
                   status: :not_ready,
                   port: %{"name" => "App", "value" => 80}
                 }
               ],
               status: %{code_extracted: false, code_fetched: false}
             }

      assert html_response(conn, 200)

      Previews.DynamicSupervisor.terminate_child(preview)
    end

    test "handles preview already launched", %{conn: conn, preview: preview, user: user} do
      log_response = {:ok, "Log line\nAnother log line"}

      # Liveness check
      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 200}}
      end)

      App.KubernetesV2Mock

      # Read that all assets exist
      |> expect(:async, Expectation.read_all(preview, Response.read_all(preview)))

      # Read pod status for catchup
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:ok,
           %{
             "items" => [
               Response.Pod.docker(preview),
               Response.Pod.preview(preview)
             ]
           }}
        )
      )

      # Read logs for catchup
      |> expect(:run, Expectation.Pod.log(preview, log_response))

      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> get(preview_path(AppWeb.Endpoint, :show), %{
          "id" => preview.id
        })

      assert conn.private[:phoenix_gon].assets[:channel_name] ==
               AppWeb.PreviewChannel.channel_name(preview)

      assert conn.private[:phoenix_gon].assets[:channel_token]

      assert conn.private[:phoenix_gon].assets[:description] == "An awesome preview"

      assert conn.private[:phoenix_gon].assets[:launch] == [:ok, :already_running]

      assert conn.private[:phoenix_gon].assets[:ports] == [
               %{
                 config: %{"name" => "App", "value" => 80},
                 id: "id39da6a4e7d8ae",
                 url: "http://id39da6a4e7d8ae.stagehand.home:32568"
               }
             ]

      assert conn.private[:phoenix_gon].assets[:preview] == %{
               logs: "Log line\nAnother log line",
               provisioning: %{docker: true},
               readiness: [
                 %{
                   status: :ready,
                   port: %{"name" => "App", "value" => 80}
                 }
               ],
               status: %{code_extracted: true, code_fetched: true}
             }

      assert html_response(conn, 200)
    end

    test "handles error in preview launch", %{conn: conn, preview: preview, user: user} do
      App.KubernetesV2Mock

      # Exists?
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      # Read before create/update operations
      |> expect(:async, Expectation.read_all(preview, Response.read_all_missing(preview)))

      # Create assets with "forbidden" error
      |> expect(:async, Expectation.start(preview, Response.create_error_forbidden(preview)))

      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> get(preview_path(AppWeb.Endpoint, :show), %{
          "id" => preview.id
        })

      assert conn.private[:phoenix_gon].assets[:channel_name] ==
               AppWeb.PreviewChannel.channel_name(preview)

      assert conn.private[:phoenix_gon].assets[:channel_token]

      assert conn.private[:phoenix_gon].assets[:description] == "An awesome preview"

      assert conn.private[:phoenix_gon].assets[:launch] ==
               [:error, :forbidden]

      assert conn.private[:phoenix_gon].assets[:ports] == [
               %{
                 config: %{"name" => "App", "value" => 80},
                 id: "id39da6a4e7d8ae",
                 url: "http://id39da6a4e7d8ae.stagehand.home:32568"
               }
             ]

      assert conn.private[:phoenix_gon].assets[:preview] == %{
               provisioning: %{docker: false},
               readiness: [
                 %{
                   status: :not_ready,
                   port: %{"name" => "App", "value" => 80}
                 }
               ],
               status: %{code_extracted: false, code_fetched: false}
             }

      assert html_response(conn, 200)
    end
  end
end
