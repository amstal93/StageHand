defmodule AppWeb.AuthorisationControllerTest do
  import App.Fixtures
  use AppWeb.ConnCase

  @success_payload %Ueberauth.Auth{
    credentials: %Ueberauth.Auth.Credentials{
      token: "abc3"
    },
    info: %Ueberauth.Auth.Info{
      email: "rodeoclash@example.com"
    },
    provider: :github,
    uid: 1234
  }

  setup do
    provider = insert(:provider, name: "github")
    %{provider: provider}
  end

  describe "GET /auth/github/callback" do
    test "with failure to auth", %{conn: conn} do
      conn =
        conn
        |> Plug.Conn.assign(:ueberauth_failure, "Reason")
        |> get("/auth/github/callback")

      assert get_flash(conn, :error) == "Failed to authenticate"
      assert redirected_to(conn) =~ "/"
    end

    test "logs expected user in and redirects to current user page", %{conn: conn} do
      conn =
        conn
        |> Plug.Conn.assign(:ueberauth_auth, @success_payload)
        |> get("/auth/github/callback")

      assert get_flash(conn, :info) == "Successfully authenticated"
      assert redirected_to(conn) =~ "/current_user"
      # TODO: Find user with email and check logged in
    end
  end
end
