defmodule AppWeb.ProjectControllerTest do
  alias App.{ProviderConnections, Repo}
  import App.Fixtures
  import AppWeb.Router.Helpers
  import Ecto.Query, only: [from: 2]
  use AppWeb.ConnCase, async: true

  setup do
    bitbucket_provider = insert(:provider, name: "bitbucket")
    github_provider = insert(:provider, name: "github")

    project = insert(:project)
    user = insert(:user)

    %{
      bitbucket_provider: bitbucket_provider,
      github_provider: github_provider,
      project: project,
      user: user
    }
  end

  describe "POST /projects" do
    test "creates an project", %{conn: conn, user: user} do
      name = "Name"

      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> post(project_path(AppWeb.Endpoint, :create), %{
          "project" => %{
            "name" => name
          }
        })

      assert conn.assigns.project.name == name
      assert conn.assigns.role.user_id == user.id
      assert conn.assigns.role.project_id == conn.assigns.project.id
      assert redirected_to(conn) == current_user_path(AppWeb.Endpoint, :show)
      assert get_flash(conn, :success) == "Created project #{name}"
    end
  end

  describe "GET /projects/:id" do
    test "renders", %{conn: conn, project: project, user: user} do
      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> get(project_path(AppWeb.Endpoint, :show, project))

      assert html_response(conn, 200)
    end
  end

  describe "POST /projects/:project_id/provider_connection" do
    test "creates a provider connection", %{
      conn: conn,
      user: user,
      project: project,
      bitbucket_provider: bitbucket_provider
    } do
      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> put(
          project_project_path(
            AppWeb.Endpoint,
            :create_provider_connection,
            project
          ),
          %{
            "project_id" => project.id,
            "provider_connection" => %{
              "provider_id" => bitbucket_provider.id,
              "user_id" => user.id
            }
          }
        )

      provider_connection =
        Repo.one(from(provider_connection in ProviderConnections.ProviderConnection, limit: 1))

      assert conn.assigns.provider_connection == provider_connection

      assert get_flash(conn, :success) ==
               "Assigned #{user.email} as the user to connect to #{bitbucket_provider.name}"

      assert redirected_to(conn) == project_path(AppWeb.Endpoint, :show, project)
    end
  end
end
