defmodule AppWeb.CurrentUserControllerTest do
  import App.Fixtures
  import AppWeb.Router.Helpers
  use AppWeb.ConnCase, async: true

  setup do
    user = insert(:user)
    %{user: user}
  end

  describe "GET /current_user" do
    test "renders", %{conn: conn, user: user} do
      conn =
        conn
        |> App.Guardian.Plug.sign_in(user)
        |> get(current_user_path(AppWeb.Endpoint, :show))

      assert html_response(conn, 200)
    end
  end
end
